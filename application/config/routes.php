<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main/beranda';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['beranda'] = 'main/beranda';
$route['kepemilikan'] = 'main/kepemilikan';
$route['profil'] = 'main/profil';
$route['kursus/(:num)'] = 'main/kursus';
$route['kursus'] = 'main/kursus';
$route['program'] = 'main/program_kursus';
$route['galeri/(:num)'] = 'main/galeri';
$route['galeri'] = 'main/galeri';
$route['berita/(:num)'] = 'main/berita';
$route['berita'] = 'main/berita';
$route['berita-detail/(:num)'] = 'main/berita_detail';
$route['berita-detail'] = 'main/berita_detail';
$route['kontak'] = 'main/kontak';
$route['daftar'] = 'main/daftar';
$route['kuis/(:num)'] = 'main/kuis';
$route['kuis'] = 'main/kursus';
$route['jawab_kuis'] = 'main/jawab_kuis';
$route['info/(:num)'] = 'main/info';
$route['info'] = 'main/info';
$route['nilai/(:num)'] = 'main/nilai';
$route['nilai'] = 'main/nilai';
$route['menu'] = 'main/menu';
$route['login'] = 'main/login';

$route['kabupaten/(:num)'] = 'main/kabupaten';
$route['kabupaten'] = 'main/kabupaten';
$route['kecamatan/(:num)'] = 'main/kecamatan';
$route['kecamatan'] = 'main/kecamatan';
$route['kelurahan/(:num)'] = 'main/kelurahan';
$route['kelurahan'] = 'main/kelurahan';

$route['pendaftaran/(:num)'] = 'admin/pendaftaran_peserta';
$route['pendaftaran'] = 'admin/pendaftaran_peserta';

$route['peserta/(:num)'] = 'admin/data_peserta';
$route['peserta'] = 'admin/data_peserta';

$route['detailp/(:num)'] = 'admin/detail_peserta';
$route['detailp'] = 'admin/detail_peserta';

$route['pengguna/(:num)'] = 'admin/pengguna';
$route['pengguna'] = 'admin/pengguna';

$route['akun/(:num)'] = 'admin/akun';
$route['akun'] = 'admin/akun';

$route['admin-galeri/(:num)'] = 'admin/galeri';
$route['admin-galeri'] = 'admin/galeri';

$route['edit-peserta/(:num)'] = 'admin/edit_peserta';
$route['edit-peserta'] = 'admin/edit_peserta';

$route['admin-program/(:num)'] = 'admin/program';
$route['admin-program'] = 'admin/program';

$route['admin-berita/(:num)'] = 'admin/berita';
$route['admin-berita'] = 'admin/berita';

