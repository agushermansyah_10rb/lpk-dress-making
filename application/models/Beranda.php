<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Model {
	private $table_prestasi = 'prestasi_lembaga';
	private $table_sarana = 'sarana_prasarana';
	private $table_tenaga = 'tenaga_pendidik';


	public function get_prestasi() {
		try {
			$query = $this->db->get($this->table_prestasi);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function get_sarana() {
		try {
			$query = $this->db->get($this->table_sarana);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function get_tenaga_pendidik() {
		try {
			$query = $this->db->get($this->table_tenaga);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 
}
?>