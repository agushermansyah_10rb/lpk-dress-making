<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Model {
	private $table = 'program';
	public $program_id = 'program_id';
	public $judul = 'judul';
	public $gambar = 'gambar';
	public $keterangan = 'keterangan';
	public $dibuat_pada = 'dibuat_pada';

	public function simpan_program($data) {
		try {
			$query = $this->db->insert($this->table, $data);
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_program($start=NULL, $rows=NULL, $keyword=NULL) {
		try {
			$this->db->order_by($this->dibuat_pada, 'desc');
			if ($keyword) {
				$this->db->like($this->judul, $keyword);
			}
			if (($start || $start == 0) && $rows) {
				$query = $this->db->get($this->table, $rows, $start);
				return $query;
			}
			else {
				$query = $this->db->get($this->table);
				return $query;
			}
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function get_program_by_id($program_id) {
		try {
			$this->db->like($this->program_id, $program_id);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function hapus_program($program_id) {
		try {
			$this->db->where($this->program_id, $program_id);
			$query = $this->db->delete($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

}
?>