<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends CI_Model {
	private $table = 'absensi';
	public $abs_id = 'abs_id';
	public $peserta_id = 'peserta_id';
	public $kehadiran = 'kehadiran';
	public $keterangan = 'keterangan';
	public $tanggal = 'tanggal';

	public function simpan_absensi($data) {
		try {
			$query = $this->db->insert($this->table, $data);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 
}
?>