<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Model {
	private $table = 'video';
	public $video_id = 'video_id';
	public $judul = 'judul';
	public $link = 'link';

	public function get_video_peserta($peserta_id, $start=NULL, $rows=NULL, $tipe=NULL) {
		try {
			$limit_ = "";
			if ($peserta_id && $tipe == 'peserta') {
				$limit_ = "LIMIT 1";
			}
			$sql = "
				SELECT * FROM ((SELECT video.*, vp.peserta_id FROM `video`
				INNER JOIN video_peserta vp ON vp.video_id = video.video_id
				WHERE vp.peserta_id = '$peserta_id')
				UNION
				(SELECT video.*, vp.peserta_id FROM `video` 
				LEFT JOIN video_peserta vp ON vp.video_id = video.video_id
				WHERE video.video_id NOT IN (SELECT video.video_id FROM `video`
				INNER JOIN video_peserta vp ON vp.video_id = video.video_id
				WHERE vp.peserta_id = '$peserta_id') GROUP by video.video_id $limit_)) a ORDER BY tingkat ASC 
			";
			if (($start || $start == 0) && $rows) {
				$sql = $sql . "LIMIT $start,$rows";
			}
			$query = $this->db->query($sql);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_video($video_id) {
		try {
			$this->db->where($this->video_id, $video_id);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 
}
?>