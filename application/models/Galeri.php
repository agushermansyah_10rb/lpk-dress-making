<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends CI_Model {
	private $table = 'galeri';
	public $g_id = 'g_id';
	public $gambar = 'gambar';
	public $keterangan = 'keterangan';
	public $dibuat_pada = 'dibuat_pada';

	public function simpan_galeri($data) {
		try {
			$query = $this->db->insert($this->table, $data);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_galeri($start=NULL, $rows=NULL, $keyword=NULL) {
		try {
			$this->db->order_by($this->dibuat_pada, 'desc');
			if ($keyword) {
				$this->db->like($this->keterangan, $keyword);
			}
			if (($start || $start == 0) && $rows) {
				$query = $this->db->get($this->table, $rows, $start);
				return $query;
			}
			else {
				$query = $this->db->get($this->table);
				return $query;
			}
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function get_galeri_by_id($g_id) {
		try {
			$this->db->like($this->g_id, $g_id);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function hapus_galeri($g_id) {
		try {
			$this->db->where($this->g_id, $g_id);
			$query = $this->db->delete($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}
}
?>