<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Model {
	private $table = 'berita';
	public $berita_id = 'berita_id';
	public $judul = 'judul';
	public $teks = 'teks';
	public $gambar = 'gambar';
	public $penulis = 'penulis';
	public $dibuat_pada = 'dibuat_pada';

	public function simpan_berita($data) {
		try {
			$query = $this->db->insert($this->table, $data);
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_berita($start=NULL, $rows=NULL, $keyword=NULL) {
		try {
			$this->db->order_by($this->dibuat_pada, 'desc');
			if ($keyword) {
				$this->db->like($this->judul, $keyword);
			}
			if (($start || $start == 0) && $rows) {
				$query = $this->db->get($this->table, $rows, $start);
			}
			else {
				$query = $this->db->get($this->table);
			}
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_berita_by_id($berita_id) {
		try {
			$this->db->where($this->berita_id, $berita_id);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function hapus_berita($berita_id) {
		try {
			$this->db->where($this->berita_id, $berita_id);
			$query = $this->db->delete($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}
}
?>