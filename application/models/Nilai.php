<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Model {
	private $table = 'nilai';
	public $n_id = 'n_id';
	public $peserta_id = 'peserta_id';
	public $kuis_id = 'kuis_id';
	public $jumlah_soal = 'jumlah_soal';
	public $jumlah_benar = 'jumlah_benar';
	public $jumlah_salah = 'jumlah_salah';
	public $skor = 'skor';
	public $dibuat_pada = 'dibuat_pada';

	public function simpan_nilai($data) {
		try {
			$query = $this->db->insert($this->table, $data);
			$last_id = $this->db->insert_id();
			return $last_id;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
	public function get_nilai($peserta_id=NULL, $start=NULL, $rows=NULL) {
		try {
			if ($peserta_id) {
				$this->db->where($this->table.'.'.$this->peserta_id, $peserta_id);
			}
			$this->db->join('kuis', 'kuis.kuis_id = nilai.kuis_id', 'INNER');
			$this->db->join('peserta', 'peserta.peserta_id = nilai.peserta_id', 'INNER');
			if (($start || $start == 0) && $rows) {
				$query = $this->db->get($this->table, $rows, $start);
			}
			else {
				$query = $this->db->get($this->table);
			}
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_nilai_by_id($n_id, $peserta_id) {
		try {
			$this->db->where($this->n_id, $n_id);
			$this->db->where($this->peserta_id, $peserta_id);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}
}
?>