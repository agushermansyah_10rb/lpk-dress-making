<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videopeserta extends CI_Model {
	private $table = 'video_peserta';
	public $video_id = 'video_id';
	public $peserta_id = 'peserta_id';

	public function simpan_video_peserta($data) {
		try {
			$query = $this->db->insert($this->table, $data);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function cek_video_peserta($video_id, $peserta_id) {
		try {
			$this->db->where($this->video_id, $video_id);
			$this->db->where($this->peserta_id, $peserta_id);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}
}
?>