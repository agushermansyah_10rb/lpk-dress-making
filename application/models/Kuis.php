<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kuis extends CI_Model {
	private $table = 'kuis';
	private $table2 = 'kuis_pj';
	private $table3 = 'kuis_peserta';

	public function get_kuis($video_id) {
		try {
			$query = $this->db->query("
				SELECT * FROM kuis
				JOIN kuis_pj ON kuis_pj.kuis_id = kuis.kuis_id
				WHERE kuis.video_id = '$video_id'
				ORDER BY rand();
			");
			return $query;
		} catch (Exception $e) {
			
		}
	}
	
	public function get_jawaban_kuis($kpj_id=NULL, $jawaban=NULL) {
		try {
			if ($kpj_id) {
				$this->db->where('kpj_id', $kpj_id);
			}
			if ($jawaban) {
				$this->db->where('jawaban', $jawaban);
			}
			$query = $this->db->get($this->table2);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function simpan_kuis_peserta($data) {
		try {
			$query = $this->db->insert($this->table3, $data);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}
	
}
?>