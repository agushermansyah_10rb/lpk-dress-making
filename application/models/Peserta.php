<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Model {
	private $table = 'peserta';
	public $peserta_id = 'peserta_id';
	public $nik = 'nik';
	public $nama_lengkap = 'nama_lengkap';
	public $email = 'email';
	public $no_telp = 'no_telp';
	public $jenis_kelamin = 'jenis_kelamin';
	public $tempat_lahir = 'tempat_lahir';
	public $tanggal_lahir = 'tanggal_lahir';
	public $alamat = 'alamat';
	public $kelurahan = 'kelurahan';
	public $kecamatan = 'kecamatan';
	public $kabupaten = 'kabupaten';
	public $provinsi = 'provinsi';
	public $pendidikan = 'pendidikan';
	public $nama_ibu = 'nama_ibu';
	public $foto_profil = 'foto_profil';
	public $jenis_kursus = 'jenis_kursus';
	public $waktu = 'waktu';
	public $status = 'status';
	public $lulus = 'lulus';
	public $keterangan = 'keterangan';
	public $dibuat_pada = 'dibuat_pada';

	public function simpan_peserta($data) {
		try {
			$query = $this->db->insert($this->table, $data);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function validasi_peserta($nik, $email, $no_telp) {
		try {
			$this->db->where($this->nik, $nik);
			$this->db->or_where($this->email, $email);
			$this->db->or_where($this->no_telp, $no_telp);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}  

	public function get_peserta($nik=NULL, $peserta_id=NULL, $start=NULL, $rows=NULL, $status=NULL, $lulus=NULL, $keyword=NULL, $not_status=NULL, $order=NULL) {
		try {
			if ($nik) {
				$this->db->where($this->nik, $nik);
			}
			if ($peserta_id) {
				$this->db->where($this->peserta_id, $peserta_id);
			}
			if ($not_status) {
				$this->db->where_not_in($this->status, $not_status);
			}
			if ($status) {
				$this->db->where($this->status, $status);
			}
			if ($lulus) {
				$this->db->where($this->lulus, $lulus);
			}
			if ($keyword) {
				$this->db->like($this->nama_lengkap, $keyword);
			}
			if ($order) {
				if ($order == 'date') {
					$this->db->order_by($this->dibuat_pada, 'asc');
				}
			}
			else {
				$this->db->order_by($this->nama_lengkap, 'asc');
			}
			$query = $this->db->get($this->table, $rows, $start);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_peserta_by_id($peserta_id) {
		try {
			$this->db->where($this->peserta_id, $peserta_id);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function update_peserta($data, $peserta_id) {
		try {
			$this->db->where($this->peserta_id, $peserta_id);
			$query = $this->db->update($this->table, $data);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function hapus_peserta($peserta_id) {
		try {
			$this->db->where($this->peserta_id, $peserta_id);
			$query = $this->db->delete($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}
}
?>