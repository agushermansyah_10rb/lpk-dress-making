<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Model {
	private $table = 'pengguna';
	public $p_id = 'p_id';
	public $username = 'username';
	public $password = 'password';
	public $tipe = 'tipe';
	public $peserta_id = 'peserta_id';

	public function auth_login($username, $password) {
		try {
			$this->db->where($this->username, $username);
			$this->db->where($this->password, $password);
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function validasi_pengguna($username, $peserta_id=NULL) {
		try {
			$this->db->where($this->username, $username);
			if ($peserta_id) {
				$this->db->or_where($this->peserta_id, $peserta_id);
			}
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function simpan_pengguna($data) {
		try {
			$query = $this->db->insert($this->table, $data);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	} 

	public function get_pengguna($start=NULL, $rows=NULL, $keyword=NULL) {
		try {
			if ($keyword) {
				$this->db->like($this->username, $keyword);
			}
			$this->db->join('peserta', 'peserta.peserta_id = pengguna.peserta_id', 'INNER');
			$this->db->order_by('peserta.nama_lengkap', 'asc');
			if (($start || $start == 0) && $rows) {
				$query = $this->db->get($this->table, $rows, $start);
			}
			else {
				$query = $this->db->get($this->table);
			}
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_pengguna_by_id($p_id) {
		try {
			$this->db->where($this->p_id, $p_id);
			$this->db->join('peserta', 'peserta.peserta_id = pengguna.peserta_id', 'INNER');
			$query = $this->db->get($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}  

	public function hapus_pengguna($p_id) {
		try {
			$this->db->where($this->p_id, $p_id);
			$query = $this->db->delete($this->table);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function update_pengguna($data, $p_id) {
		try {
			$this->db->or_where($this->p_id, $p_id);
			$query = $this->db->update($this->table, $data);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

}
?>