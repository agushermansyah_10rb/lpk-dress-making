<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daerah extends CI_Model {
	private $table_provinsi = 'provinsi';
	private $table_kabupaten = 'kabupaten';
	private $table_kecamatan = 'kecamatan';
	private $table_kelurahan = 'kelurahan';
	
	public function get_provinsi() {
		try {
			$this->db->order_by('nama', 'desc');
			$query = $this->db->get($this->table_provinsi);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_kabupaten($id_prov) {
		try {
			$this->db->where('id_prov', $id_prov);
			$this->db->order_by('nama', 'desc');
			$query = $this->db->get($this->table_kabupaten);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_kecamatan($id_kab) {
		try {
			$this->db->where('id_kab', $id_kab);
			$this->db->order_by('nama', 'desc');
			$query = $this->db->get($this->table_kecamatan);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function get_kelurahan($id_kec) {
		try {
			$this->db->where('id_kec', $id_kec);
			$this->db->order_by('nama', 'desc');
			$query = $this->db->get($this->table_kelurahan);
			return $query;
		} catch (Exception $e) {
			throw $e;
		}
	}
}
?>