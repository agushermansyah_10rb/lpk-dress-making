<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('pengguna');
	}

	public function auth_login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$result = $this->pengguna->auth_login($username, $password);
		if ($result->num_rows() > 0) {
			$data = json_encode($result->row());
			$data = json_decode($data, true);
			$data['status'] = 'login';
			$this->session->set_userdata($data);
			redirect(site_url('kursus'));
		}
		else {
			redirect(site_url('login?status=gagal'));
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url('login'));
	}
}

?>