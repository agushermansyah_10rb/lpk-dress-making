<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pendaftaran extends CI_Controller {
	
	public function daftar() {
		$this->load->model('peserta');
		$data = $this->input->post();
		$nik = $data['nik'];
		$email = $data['email'];
		$no_telp = $data['no_telp'];

		$error = "";

		if (!preg_match("/[0-9]{16}/", $nik)) {
			// echo "NIK tidak benar";
			$error = "1";
		}
		else if (!preg_match("/@/", $email)) {
			// echo "Email tidak benar";
			$error = "2";
		}
		else if (!preg_match("/[0-9]{6,13}/", $no_telp)) {
			// echo "No telp tidak benar";
			$error = "3";
		}
		else {
			$validasi = $this->peserta->validasi_peserta($nik, $email, $no_telp)->row();
			if ($validasi) {
				if ($validasi->nik == $nik) {
					// echo "NIK sudah terdaftar";
					$error = "4";
				}
				else if ($validasi->email == $email) {
					// echo "Email sudah terdaftar";
					$error = "5";
				}
				else if ($validasi->no_telp == $no_telp) {
					// echo "No telp sudah terdaftar";
					$error = "6";
				}
			} 
			else {
				$peserta = array(
					$this->peserta->nik => $nik, 
					$this->peserta->nama_lengkap => $data['nama_lengkap'], 
					$this->peserta->email => $email, 
					$this->peserta->no_telp => $no_telp, 
					$this->peserta->jenis_kelamin => $data['jenis_kelamin'], 
					$this->peserta->tempat_lahir => $data['tempat_lahir'], 
					$this->peserta->tanggal_lahir => $data['tanggal_lahir'], 
					$this->peserta->alamat => $data['alamat'], 
					$this->peserta->kelurahan => preg_replace('/[0-9]+\|/', '', $data['kelurahan']), 
					$this->peserta->kecamatan => preg_replace('/[0-9]+\|/', '', $data['kecamatan']), 
					$this->peserta->kabupaten => preg_replace('/[0-9]+\|/', '', $data['kabupaten']), 
					$this->peserta->provinsi => preg_replace('/[0-9]+\|/', '', $data['provinsi']), 
					$this->peserta->pendidikan => $data['pendidikan'], 
					$this->peserta->nama_ibu => $data['nama_ibu'], 
					$this->peserta->jenis_kursus => $data['jenis_kursus'], 
					$this->peserta->waktu => $data['waktu_kursus'], 
				);
				if (!empty($_FILES["image"]["name"])) {
					$respon = $this->_uploadImage();
					$status = $respon[0];
					$pesan = $respon[1];
					$file_name = $respon[2];
					if ($status) {
						$peserta[$this->peserta->foto_profil] = $file_name;
						$this->peserta->simpan_peserta($peserta);
					} 
					else {
						if (preg_match("/upload is not allowed/", $pesan)) {
							// echo "Format gambar tidak sesuai ketentuan (.jpeg/.jpg/.png)";
							$error = "7";
						}
						else if (preg_match("/upload is larger/", $pesan)) {
							// echo "Ukuran gambar terlalu besar (maksimal 1 MB)";
							$error = "8";
						}
						else {
							// echo "Gagal mengupload gambar";
							$error = "9";
						}
					}
				}
				else {
					$this->peserta->simpan_peserta($peserta);
				}
			}
		}

		if (!$error) {
			redirect(site_url('daftar?status=sukses'));
		}
		else {
			redirect(site_url('daftar?status=gagal&error='.$error));
		}
	}

	private function _uploadImage() {
	    $config['upload_path']          = './asset/images/pendaftaran/';
	    $config['allowed_types']        = 'jpeg|jpg|png';
	    $config['file_name']            = uniqid();
	    $config['overwrite']			= true;
	    $config['max_size']             = 512; // 0.5MB
	    // $config['max_width']            = 1024;
	    // $config['max_height']           = 768;

	    $this->load->library('upload', $config);

	    if ($this->upload->do_upload('image')) {
	        return array(true, 'success', $this->upload->data("file_name"));
	    }
	    $pesan = $this->upload->display_errors();
	    return array(false, $pesan, "default.jpg");
	}

}
?>