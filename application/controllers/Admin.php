<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function pendaftaran_peserta() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('peserta');

				// Jika ada pencarian data 
				$cari = $this->input->post();
				$keyword = $cari ? $cari['cari'] : NULL;
				if (!$keyword) {
					if ($_GET) {
						if (isset($_GET["q"])) {
							$keyword = $_GET["q"];
						}
					}
				}

		        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		        $rows = 10;
		        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

		        $data['data'] = $this->peserta->get_peserta(NULL, NULL, $start, $rows, 'proses', NULL, $keyword, NULL, 'date')->result();
		        $data['total'] = $this->peserta->get_peserta(NULL, NULL, NULL, NULL, 'proses', NULL, $keyword)->num_rows();
		        $data['keyword'] = $keyword;
				$this->load->view('pendaftaran', $data);	
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function tambah_pengguna_pendaftaran() {
		$error = $this->tambah_pengguna();
		if (!$error) {
				redirect(site_url('pendaftaran?status=sukses'));
			}
		else {
			redirect(site_url('pendaftaran?status=gagal&error='.$error));
		}
	}

	public function tambah_pengguna_peserta() {
		$error = $this->tambah_pengguna();
		if (!$error) {
				redirect(site_url('peserta?status=sukses'));
			}
		else {
			redirect(site_url('peserta?status=gagal&error='.$error));
		}
	}

	public function tambah_pengguna() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('pengguna');
			$this->load->model('peserta');

			$data = $this->input->post();
			$peserta_id = $data["peserta_id"];
			$username = $data["username"];
			$password = $data["password"];
			$error = "";
			
			$validasi = $this->pengguna->validasi_pengguna($username, $peserta_id)->row();
			if ($validasi > 0) {
				$error = "1";
			}
			else {
				$pengguna = array(
					$this->pengguna->username => $username, 
					$this->pengguna->password => $password, 
					$this->pengguna->tipe => 'peserta', 
					$this->pengguna->peserta_id => $peserta_id, 
				);
				$peserta = array(
					$this->peserta->status => 'disetujui'
				);
				$this->pengguna->simpan_pengguna($pengguna);
				$this->peserta->update_peserta($peserta, $peserta_id);
			}
			return $error;
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function tambah_keterangan() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('peserta');

			$data = $this->input->post();
			$peserta_id = $data["peserta_id"];
			$keterangan = $data["keterangan"];
			$error = "";

			$peserta = array(
				$this->peserta->status => 'ditolak',
				$this->peserta->keterangan => $keterangan
			);
			$this->peserta->update_peserta($peserta, $peserta_id);

			if (!$error) {
				redirect(site_url('pendaftaran?status=sukses'));
			}
			else {
				redirect(site_url('pendaftaran?status=gagal&error='.$error));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}
	
	public function detail_peserta() {
		$userdata = $this->session->userdata();
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('peserta');
		        $peserta_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

		        $data['data'] = $this->peserta->get_peserta_by_id($peserta_id)->row();
				$this->load->view('detail_peserta', $data);	
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function data_peserta() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('peserta');

				// Jika ada pencarian data 
				$filter = $this->input->post();
				$keyword = $filter ? $filter['cari'] : NULL;
				$stat = $filter ? $filter['stat'] : NULL;
				if (!$keyword) {
					if ($_GET) {
						if (isset($_GET["q"])) {
							$keyword = $_GET["q"];
						}
						if (isset($_GET["stat"])) {
							$stat = $_GET["stat"];
						}
					}
				}

		        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		        $rows = 10;
		        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

		        $data['data'] = $this->peserta->get_peserta(NULL, NULL, $start, $rows, $stat, NULL, $keyword, 'proses')->result();
		        $data['total'] = $this->peserta->get_peserta(NULL, NULL, NULL, NULL, $stat, NULL, $keyword, 'proses')->num_rows();
		        $data['keyword'] = $keyword;
		        $data['stat'] = $stat;
				$this->load->view('peserta', $data);	
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function hapus_peserta() {
		$userdata = $this->session->userdata();
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('peserta');

		        $peserta_id = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		  
		        $this->peserta->hapus_peserta($peserta_id);
		        redirect(site_url('detailp/'.$peserta_id.'?status=sukses'));
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function edit_peserta() {
		$userdata = $this->session->userdata();
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('peserta');
				$this->load->model('daerah');
		
		        $peserta_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		        $data['data']= $this->peserta->get_peserta_by_id($peserta_id)->row();
		        $data['provinsi'] = $this->daerah->get_provinsi()->result();
		        $this->load->view('edit_peserta', $data);
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function update_peserta() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('peserta');
			$data = $this->input->post();
			$nik = $data['nik'];
			$peserta_id = $data['peserta_id'];
			$email = $data['email'];
			$no_telp = $data['no_telp'];
			$foto_profil = $data['foto_profil'];

			$error = "";

			if (!preg_match("/[0-9]{16}/", $nik)) {
				// echo "NIK tidak benar";
				$error = "1";
			}
			else if (!preg_match("/@/", $email)) {
				// echo "Email tidak benar";
				$error = "2";
			}
			else if (!preg_match("/[0-9]{6,13}/", $no_telp)) {
				// echo "No telp tidak benar";
				$error = "3";
			}
			else {
				$validasi = $this->peserta->validasi_peserta($nik, $email, $no_telp)->row();
				if ($validasi) {
					if ($validasi->nik == $nik && $validasi->peserta_id != $peserta_id) {
						// echo "NIK sudah terdaftar";
						$error = "4";
					}
					else if ($validasi->email == $email && $validasi->peserta_id != $peserta_id) {
						// echo "Email sudah terdaftar";
						$error = "5";
					}
					else if ($validasi->no_telp == $no_telp && $validasi->peserta_id != $peserta_id) {
						// echo "No telp sudah terdaftar";
						$error = "6";
					}
				}

				if ($simpan) {
					$this->pengguna->update_pengguna($pengguna, $p_id);

					$peserta = array();
					if (strtolower($nama_lengkap) != strtolower($nama_lengkap_old)) {
						$peserta[$this->peserta->nama_lengkap] = $nama_lengkap;
					}
					if ($new_file_name) {
						$peserta[$this->peserta->foto_profil] = $new_file_name;
						unlink("./asset/images/pendaftaran/".$foto_profil);
					}
					if ($peserta) {
						print_r($peserta);
						$this->peserta->update_peserta($peserta, $peserta_id);
					}
				}

				$simpan = false;
				$new_file_name = "";

				if (!$error) {
					$peserta = array(
						$this->peserta->nik => $nik, 
						$this->peserta->nama_lengkap => $data['nama_lengkap'], 
						$this->peserta->email => $email, 
						$this->peserta->no_telp => $no_telp, 
						$this->peserta->jenis_kelamin => $data['jenis_kelamin'], 
						$this->peserta->tempat_lahir => $data['tempat_lahir'], 
						$this->peserta->tanggal_lahir => $data['tanggal_lahir'], 
						$this->peserta->alamat => $data['alamat'], 
						$this->peserta->kelurahan => preg_replace('/[0-9]+\|/', '', $data['kelurahan']), 
						$this->peserta->kecamatan => preg_replace('/[0-9]+\|/', '', $data['kecamatan']), 
						$this->peserta->kabupaten => preg_replace('/[0-9]+\|/', '', $data['kabupaten']), 
						$this->peserta->provinsi => preg_replace('/[0-9]+\|/', '', $data['provinsi']), 
						$this->peserta->pendidikan => $data['pendidikan'], 
						$this->peserta->nama_ibu => $data['nama_ibu'], 
						$this->peserta->jenis_kursus => $data['jenis_kursus'], 
						$this->peserta->waktu => $data['waktu_kursus'], 
					);
					if (!empty($_FILES["image"]["name"])) {
						$respon = $this->_uploadImage('./asset/images/pendaftaran/', 512);
						$stat = $respon[0];
						$pesan = $respon[1];
						$file_name = $respon[2];
						if ($stat) {
							$new_file_name = $file_name;
							$simpan = true;
						} 
						else {
							if (preg_match("/upload is not allowed/", $pesan)) {
								// echo "Format gambar tidak sesuai ketentuan (.jpeg/.jpg/.png)";
								$error = "7";
							}
							else if (preg_match("/upload is larger/", $pesan)) {
								// echo "Ukuran gambar terlalu besar (maksimal 1 MB)";
								$error = "8";
							}
							else {
								// echo "Gagal mengupload gambar";
								$error = "9";
							}
						}
					}
					else {
						$simpan = true;
					}
				}

				if ($simpan) {
					if ($new_file_name) {
						$peserta[$this->peserta->foto_profil] = $new_file_name;
						unlink("./asset/images/pendaftaran/".$foto_profil);
					}
					$this->peserta->update_peserta($peserta, $peserta_id);
				}
			}

			if (!$error) {
				redirect(site_url('edit-peserta/'.$peserta_id.'?status=sukses'));
			}
			else {
				redirect(site_url('edit-peserta/'.$peserta_id.'?status=gagal&error='.$error));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	// Pengguna
	public function pengguna() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('pengguna');

				// Jika ada pencarian data 
				$cari = $this->input->post();
				$keyword = $cari ? $cari['cari'] : NULL;
				if (!$keyword) {
					if ($_GET) {
						if (isset($_GET["q"])) {
							$keyword = $_GET["q"];
						}
					}
				}

		        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		        $rows = 10;
		        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

		        $data['data'] = $this->pengguna->get_pengguna($start, $rows, $keyword)->result();
		        $data['total'] = $this->pengguna->get_pengguna(NULL, NULL, $keyword)->num_rows();
		        $data['keyword'] = $keyword;
				$this->load->view('pengguna', $data);	
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function hapus_pengguna() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('pengguna');

		        $p_id = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		  
		        $this->pengguna->hapus_pengguna($p_id);
		        redirect(site_url('pengguna?status=sukses'));
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	// Akun
	public function akun() {
		$userdata = $this->session->userdata();
		$pid = $userdata['p_id'];
		$peserta_id = $userdata['peserta_id'];
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('pengguna');

			if($tipe == "admin") {
	        	$p_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : $pid;
			}
			else {
				$p_id = $pid;
			}
	        $data['data'] = $this->pengguna->get_pengguna_by_id($p_id)->row();
			$this->load->view('akun', $data);	
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function edit_akun() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('pengguna');
			$this->load->model('peserta');

			$data = $this->input->post();
			$p_id = $data["p_id"];
			$peserta_id = $data["peserta_id"];
			$nama_lengkap_old = $data["nama_lengkap_old"];
			$nama_lengkap = $data["nama_lengkap"];
			$foto_profil = $data["foto_profil"];
			$username = $data["username"];
			$new_password = $data["new_password"];

			$error = "";
			$validasi = $this->pengguna->validasi_pengguna($username)->row();
			if ($validasi) {
				if ($validasi->p_id != $p_id) {
					// echo "Username sudah ada"
					$error = "1";
				}
			} 

			if (!$error) {
				$pengguna = array(
					$this->pengguna->username => $username
				);
				if ($new_password) {
					$pengguna[$this->pengguna->password] = $new_password;
				}
				
				$peserta = array();

				$simpan = false;
				$new_file_name = ""; 

				if (!empty($_FILES["image"]["name"])) {
					$respon = $this->_uploadImage('./asset/images/pendaftaran/', 512);
					$status = $respon[0];
					$pesan = $respon[1];
					$file_name = $respon[2];
					if ($status) {
						$new_file_name = $file_name;
						$simpan = true;
					} 
					else {
						if (preg_match("/upload is not allowed/", $pesan)) {
							// echo "Format gambar tidak sesuai ketentuan (.jpeg/.jpg/.png)";
							$error = "2";
						}
						else if (preg_match("/upload is larger/", $pesan)) {
							// echo "Ukuran gambar terlalu besar (maksimal 1/2 MB)";
							$error = "3";
						}
						else {
							// echo "Gagal mengupload gambar";
							$error = "4";
						}
					}
				}
				else {
					$simpan = true;
				}

				if ($simpan) {
					$this->pengguna->update_pengguna($pengguna, $p_id);

					$peserta = array();
					if (strtolower($nama_lengkap) != strtolower($nama_lengkap_old)) {
						$peserta[$this->peserta->nama_lengkap] = $nama_lengkap;
					}
					if ($new_file_name) {
						$peserta[$this->peserta->foto_profil] = $new_file_name;
						unlink("./asset/images/pendaftaran/".$foto_profil);
					}
					if ($peserta) {
						print_r($peserta);
						$this->peserta->update_peserta($peserta, $peserta_id);
					}
				}
			}

			if (!$error) {
				redirect(site_url('akun/'.$p_id.'?status=sukses'));
			}
			else {
				redirect(site_url('akun/'.$p_id.'?status=gagal&error='.$error));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	private function _uploadImage($path, $size) {
	    $config['upload_path']          = $path;
	    $config['allowed_types']        = 'jpeg|jpg|png';
	    $config['file_name']            = uniqid();
	    $config['overwrite']			= true;
	    $config['max_size']             = $size;
	    // $config['max_width']            = 1024;
	    // $config['max_height']           = 768;

	    $this->load->library('upload', $config);

	    if ($this->upload->do_upload('image')) {
	        return array(true, 'success', $this->upload->data("file_name"));
	    }
	    $pesan = $this->upload->display_errors();
	    return array(false, $pesan, "default.jpg");
	}

	// Galeri
	public function galeri() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('galeri');

			// Jika ada pencarian data 
			$cari = $this->input->post();
			$keyword = $cari ? $cari['cari'] : NULL;
			if (!$keyword) {
				if ($_GET) {
					if (isset($_GET["q"])) {
						$keyword = $_GET["q"];
					}
				}
			}

	        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	        $rows = 10;
	        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

	        $data['data'] = $this->galeri->get_galeri($start, $rows, $keyword)->result();
	        $data['total'] = $this->galeri->get_galeri(NULL, NULL, $keyword)->num_rows();
	        $data['keyword'] = $keyword;
			$this->load->view('ad_galeri', $data);
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function tambah_galeri() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('galeri');

			$data = $this->input->post();
			$keterangan = $data["keterangan"];

			$galeri = array(
				$this->galeri->keterangan => $keterangan
			);

			if (!empty($_FILES["image"]["name"])) {
				$respon = $this->_uploadImage('./asset/images/galeri/', 1024);
				$status = $respon[0];
				$pesan = $respon[1];
				$file_name = $respon[2];
				if ($status) {
					$galeri[$this->galeri->gambar] = $file_name;
					$this->galeri->simpan_galeri($galeri);
				} 
				else {
					if (preg_match("/upload is not allowed/", $pesan)) {
						// echo "Format gambar tidak sesuai ketentuan (.jpeg/.jpg/.png)";
						$error = "1";
					}
					else if (preg_match("/upload is larger/", $pesan)) {
						// echo "Ukuran gambar terlalu besar (maksimal 1/2 MB)";
						$error = "2";
					}
					else {
						// echo "Gagal mengupload gambar";
						$error = "3";
					}
				}
			}
			else {
				$error = "4";
			}

			if (!$error) {
				redirect(site_url('admin-galeri?status=sukses&t=a'));
			}
			else {
				redirect(site_url('admin-galeri?status=gagal&t=a&error='.$error));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function hapus_galeri() {
		$userdata = $this->session->userdata();
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('galeri');

		        $p_id = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		        $validasi = $this->galeri->get_galeri_by_id($p_id)->row();
		        if ($validasi) {
		        	$gambar = $validasi->gambar;
		        	if ($gambar) {
		        		unlink("./asset/images/galeri/".$gambar);
		        	}
		        	$this->galeri->hapus_galeri($p_id);
		       		redirect(site_url('admin-galeri?status=sukses&t=b'));
		        }
		        else {
		        	redirect(site_url('admin-galeri?status=gagal&t=b&error=1'));
		        }
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	// Program
	public function program() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('program');

			// Jika ada pencarian data 
			$cari = $this->input->post();
			$keyword = $cari ? $cari['cari'] : NULL;
			if (!$keyword) {
				if ($_GET) {
					if (isset($_GET["q"])) {
						$keyword = $_GET["q"];
					}
				}
			}

	        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	        $rows = 10;
	        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

	        $data['data'] = $this->program->get_program($start, $rows, $keyword)->result();
	        $data['total'] = $this->program->get_program(NULL, NULL, $keyword)->num_rows();
	        $data['keyword'] = $keyword;
			$this->load->view('ad_program', $data);
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function tambah_program() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('program');

			$data = $this->input->post();
			$judul = $data["judul"];
			$keterangan = $data["keterangan"];

			$program = array(
				$this->program->judul => $judul,
				$this->program->keterangan => $keterangan
			);

			if (!empty($_FILES["image"]["name"])) {
				$respon = $this->_uploadImage('./asset/images/program/', 512);
				$status = $respon[0];
				$pesan = $respon[1];
				$file_name = $respon[2];
				if ($status) {
					$program[$this->program->gambar] = $file_name;
					$this->program->simpan_program($program);
				} 
				else {
					if (preg_match("/upload is not allowed/", $pesan)) {
						// echo "Format gambar tidak sesuai ketentuan (.jpeg/.jpg/.png)";
						$error = "1";
					}
					else if (preg_match("/upload is larger/", $pesan)) {
						// echo "Ukuran gambar terlalu besar (maksimal 1/2 MB)";
						$error = "2";
					}
					else {
						// echo "Gagal mengupload gambar";
						$error = "3";
					}
				}
			}
			else {
				$error = "4";
			}

			if (!$error) {
				redirect(site_url('admin-program?status=sukses&t=a'));
			}
			else {
				redirect(site_url('admin-program?status=gagal&t=a&error='.$error));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function hapus_program() {
		$userdata = $this->session->userdata();
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('program');

		        $program_id = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		        $validasi = $this->program->get_program_by_id($program_id)->row();
		        if ($validasi) {
		        	$gambar = $validasi->gambar;
		        	if ($gambar) {
		        		unlink("./asset/images/program/".$gambar);
		        	}
		        	$this->program->hapus_program($program_id);
		       		redirect(site_url('admin-program?status=sukses&t=b'));
		        }
		        else {
		        	redirect(site_url('admin-program?status=gagal&t=b&error=1'));
		        }
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	// Berita
	public function berita() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('berita');

			// Jika ada pencarian data 
			$cari = $this->input->post();
			$keyword = $cari ? $cari['cari'] : NULL;
			if (!$keyword) {
				if ($_GET) {
					if (isset($_GET["q"])) {
						$keyword = $_GET["q"];
					}
				}
			}

	        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	        $rows = 10;
	        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

	        $data['data'] = $this->berita->get_berita($start, $rows, $keyword)->result();
	        $data['total'] = $this->berita->get_berita(NULL, NULL, $keyword)->num_rows();
	        $data['keyword'] = $keyword;
			$this->load->view('ad_berita', $data);
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function tambah_berita() {
		$userdata = $this->session->userdata();
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('berita');

			$data = $this->input->post();
			$judul = $data["judul"];
			$penulis = $data["penulis"];
			$teks = $data["konten"];

			$berita = array(
				$this->berita->judul => $judul,
				$this->berita->teks => $teks
			);

			if ($penulis) {
				$berita[$this->berita->penulis] = $penulis;
			}

			if (!empty($_FILES["image"]["name"])) {
				$respon = $this->_uploadImage('./asset/images/berita/', 512);
				$status = $respon[0];
				$pesan = $respon[1];
				$file_name = $respon[2];
				if ($status) {
					$berita[$this->berita->gambar] = $file_name;
					$this->berita->simpan_berita($berita);
				} 
				else {
					if (preg_match("/upload is not allowed/", $pesan)) {
						// echo "Format gambar tidak sesuai ketentuan (.jpeg/.jpg/.png)";
						$error = "1";
					}
					else if (preg_match("/upload is larger/", $pesan)) {
						// echo "Ukuran gambar terlalu besar (maksimal 1/2 MB)";
						$error = "2";
					}
					else {
						// echo "Gagal mengupload gambar";
						$error = "3";
					}
				}
			}
			else {
				$this->berita->simpan_berita($berita);
			}

			if (!$error) {
				redirect(site_url('admin-berita?status=sukses&t=a'));
			}
			else {
				redirect(site_url('admin-berita?status=gagal&t=a&error='.$error));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function hapus_berita() {
		$userdata = $this->session->userdata();
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->model('berita');

		        $berita_id = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		        $validasi = $this->berita->get_berita_by_id($berita_id)->row();
		        if ($validasi) {
		        	$gambar = $validasi->gambar;
		        	if ($gambar) {
		        		unlink("./asset/images/berita/".$gambar);
		        	}
		        	$this->berita->hapus_berita($berita_id);
		       		redirect(site_url('admin-berita?status=sukses&t=b'));
		        }
		        else {
		        	redirect(site_url('admin-berita?status=gagal&t=b&error=1'));
		        }
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

}
?>