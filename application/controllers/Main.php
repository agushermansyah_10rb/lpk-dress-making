<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	public function login() {
		$this->load->view('login');
	}

	public function beranda() {
		$this->load->view('beranda');
	}

	public function kepemilikan() {
		$this->load->model('beranda');
		$data["prestasi"] = $this->beranda->get_prestasi()->result();
		$data["sarana"] = $this->beranda->get_sarana()->result();
		$data["tenaga"] = $this->beranda->get_tenaga_pendidik()->result();
		$this->load->view('kepemilikan', $data);
	}

	public function profil() {
		$this->load->view('profil');
	}

	public function kontak() {
		$this->load->view('kontak');
	}

	// Berita
	public function berita() {
		$this->load->model('berita');

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $rows = 5;
        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

        $data['data'] = $this->berita->get_berita($start, $rows)->result();
        $data['total'] = $this->berita->get_berita()->num_rows();
		$this->load->view('berita', $data);
	}

	// Berita
	public function berita_detail() {
		$this->load->model('berita');
       	$berita_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data['data'] = $this->berita->get_berita_by_id($berita_id)->row();
		$this->load->view('berita_detail', $data);
	}


	// Galeri
	public function galeri() {
		$this->load->model('galeri');

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $rows = 9;
        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

        $data['data'] = $this->galeri->get_galeri($start, $rows)->result();
        $data['total'] = $this->galeri->get_galeri()->num_rows();
		$this->load->view('galeri', $data);
	}

	// Kursus
	public function kursus() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$status = $userdata['status'];
		$tipe = $userdata['tipe'];

		if($status == 'login') {
			$this->load->model('video');

	        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	        $rows = 5;
	        $start = ($page > 0) ? (($page - 1) * $rows) : 0;
	       

	        $data['data'] = $this->video->get_video_peserta($peserta_id, $start, $rows, $tipe)->result();     
	        $data['total'] = $this->video->get_video_peserta($peserta_id, NULL, NULL, $tipe)->num_rows();
	        $data['tipe'] = $tipe;
	        $data['peserta_id'] = $peserta_id;
			
			$this->load->view('kursus', $data);
		}
		else {
			redirect(site_url('login'));
		}
	}

	// Program Kursus
	public function program_kursus() {
		$this->load->model('program');
        $data['data'] = $this->program->get_program()->result();
		$this->load->view('program', $data);
	}

	public function kuis() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('kuis');
			$this->load->model('video');
			$this->load->model('videopeserta');

			$video_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
			$cek = $this->videopeserta->cek_video_peserta($video_id, $peserta_id)->num_rows();
			if ($cek > 0) {
				if ($_GET) {
		            if (isset($_GET['status'])) {
		                $status = $_GET['status']; 
		                $nid = '';
		                if (isset($_GET['nid'])) {
		                	$nid = $_GET['nid'];
		                }
		                $params = "status=$status";
		                if ($nid) {
		                	$params = $params."&nid=$nid";
		                } 
		                redirect(site_url('kursus?'.$params));
		            }
		            redirect(site_url('kursus'));
		        }
				redirect(site_url('kursus'));
			}
			else {
				$data['data'] = $this->kuis->get_kuis($video_id)->result();
				$data['total'] = $this->kuis->get_kuis($video_id)->num_rows();
				$data['video'] = $this->video->get_video($video_id)->row();
				$this->load->view('kuis', $data);
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	public function jawab_kuis() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('kuis');
			$this->load->model('nilai');
			$this->load->model('videopeserta');
			$this->load->model('absensi');

			$data = $this->input->post();
			$ids = $data['id'];
			$jumlah = $data['jumlah'];
			$kuis_id = $data['kuis_id'];
			$video_id = $data['video_id'];
			$pilihan = isset($data['pilihan']) ?  $data['pilihan'] : '';

			$score = 0;
			$benar = 0;
			$salah = 0;
			$kosong = 0;

			if ($pilihan) {
				for ($i=0; $i<$jumlah; $i++) {
					$nomor = $ids[$i];

					$jawaban_peserta = array(
						'peserta_id' => $peserta_id, 
						'kpj_id' => $nomor,
					);

					if(empty($pilihan[$nomor])) {
						$jawaban_peserta['status'] = 'kosong';
						$kosong += 1;
					} else {
						$jawaban = $pilihan[$nomor];
						$isi = $this->kuis->get_jawaban_kuis($nomor, $jawaban)->num_rows();
						if ($isi > 0) {
							$jawaban_peserta['status'] = 'benar';
							$jawaban_peserta['jawaban'] = strtolower($jawaban);
							$benar += 1;
						} else {
							$jawaban_peserta['status'] = 'salah';
							$jawaban_peserta['jawaban'] = strtolower($jawaban);
							$salah += 1;
						}
					}
					$this->kuis->simpan_kuis_peserta($jawaban_peserta);
				}
			} else {
				$kosong = $jumlah;
			}

			// Simpan Skor
			$score = 100 / $jumlah * $benar;
			$hasil = number_format($score, 1);
			$nilai = array(
				$this->nilai->peserta_id => $peserta_id, 
				$this->nilai->kuis_id => $kuis_id,
				$this->nilai->jumlah_soal => $jumlah,
				$this->nilai->jumlah_benar => $benar,
				$this->nilai->jumlah_salah => ($salah + $kosong),
				$this->nilai->skor => $hasil,
			);
			$nilai_id = $this->nilai->simpan_nilai($nilai);

			// Simpan Video Peserta
			$video_peserta = array(
				$this->videopeserta->video_id => $video_id, 
				$this->videopeserta->peserta_id => $peserta_id
			);
			$this->videopeserta->simpan_video_peserta($video_peserta);

			// Simpan Absensi
			$data_absensi = array(
				$this->absensi->peserta_id => $peserta_id, 
				$this->absensi->kehadiran => '1'
			);
			$this->absensi->simpan_absensi($data_absensi);

			redirect(site_url('kuis/'.$kuis_id.'?status=sukses&nid='.$nilai_id));
		}
		else {
			redirect(site_url('login'));
		}
	}

	// Pendaftaran Online
	public function kabupaten() {
		$this->load->model('daerah');
		$id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data = $this->daerah->get_kabupaten($id)->result();
		echo json_encode($data);
	}

	public function kecamatan() {
		$this->load->model('daerah');
		$id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data = $this->daerah->get_kecamatan($id)->result();
		echo json_encode($data);
	}

	public function kelurahan() {
		$this->load->model('daerah');
		$id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data = $this->daerah->get_kelurahan($id)->result();
		echo json_encode($data);
	}

	public function daftar() {
		$this->load->model('daerah');
		$data['provinsi'] = $this->daerah->get_provinsi()->result();
		$this->load->view('daftar', $data);
	}

	public function menu() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$tipe = $userdata['tipe'];
		$status = $userdata['status'];

		if($status == 'login') {
			if ($tipe == 'admin') {
				$this->load->view('menu');	
			}
			else {
				redirect(site_url('beranda'));
			}
		}
		else {
			redirect(site_url('login'));
		}
	}

	// Info
	public function info() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$status = $userdata['status'];
		if($status == 'login') {
			$this->load->model('nilai');
			$nilai_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
			$data['data'] = $this->nilai->get_nilai_by_id($nilai_id, $peserta_id)->row();
			$this->load->view('info', $data);
		}
		else {
			redirect(site_url('login'));
		}
	}

	// Data Nilai
	public function nilai() {
		$userdata = $this->session->userdata();
		$peserta_id = $userdata['peserta_id'];
		$status = $userdata['status'];

		if($status == 'login') {
			$this->load->model('nilai');

			if ($_GET) {
				if (isset($_GET["pid"])) {
					$peserta_id = $_GET["pid"];
					$data['pid'] = $peserta_id;
				}
			}

	        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
	        $rows = 10;
	        $start = ($page > 0) ? (($page - 1) * $rows) : 0;

	        $data['data'] = $this->nilai->get_nilai($peserta_id, $start, $rows)->result();
	        $data['total'] = $this->nilai->get_nilai($peserta_id)->num_rows();
			$this->load->view('nilai', $data);
		}
		else {
			redirect(site_url('login'));
		}
	}

}

?>