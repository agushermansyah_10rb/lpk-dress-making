<!DOCTYPE html>
<html lang="en">
<head>
<title>Dress Making</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Course Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/styles/login.css"); ?>">
<script src="<?php echo base_url("asset/js/sweetalert/sweetalert.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/alert/alert.js"); ?>"></script>
<link rel="icon" type="image/png" href="<?php echo base_url("asset/images/lpk/logo.jpg"); ?>">
</head>
<body>

<!-- Response status -->
<?php 
    if ($_GET) {
        if (isset($_GET['status'])) {
            $status = $_GET['status']; 
            if ($status == 'gagal') {
                echo "<script type='text/javascript'>failed('Nama Pengguna Atau Kata Sandi Salah', 'login');</script>";
            } 
        }
    }
?>
<div class="login-page">
	<div class="form">
    	<h1>LKP/LPK Dress Making</h1>
    	<form class="login-form" action="<?php echo site_url('login/auth_login'); ?>" method="POST">
      		<input class="input" type="text" name="username" placeholder="Email"/>
      		<input class="input" type="password" name="password" placeholder="Kata Sandi"/>
      		<input class="submit" type="submit" value="Masuk">
      		<p class="message">Belum daftar? <a href="<?php echo base_url("daftar"); ?>">Daftar sekarang!</a></p>
    	</form>
  	</div>
</div>
</body>
</html>