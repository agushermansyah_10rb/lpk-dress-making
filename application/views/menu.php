<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Menu Admin</h1>
			</div>
		</div>
	</div>

	<!-- Galeri -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Menu Admin</h1>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center">
				<div class="col-lg-3 hero_box_col">
					<a href="<?php echo site_url('pendaftaran'); ?>">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<div class="hero_box_content">
								<div class="row">
									<div class="col-lg-4"><i class="fa fa-list-alt"></i></div>
									<div class="col-lg-8">
										<h2 style="color: #FFFFFF">Registrasi</h2>
										<div class="hero_box_link">Lihat Detail</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>

				<div class="col-lg-3 hero_box_col">
					<a href="<?php echo site_url('peserta'); ?>">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<div class="hero_box_content">
								<div class="row">
									<div class="col-lg-4"><i class="far fa-user-circle"></i></div>
									<div class="col-lg-8">
										<h2 style="color: #FFFFFF">Peserta</h2>
										<div class="hero_box_link">Lihat Detail</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>

				<div class="col-lg-3 hero_box_col">
					<a href="<?php echo site_url('pengguna'); ?>">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<div class="hero_box_content">
								<div class="row">
									<div class="col-lg-4"><i class="fas fa-user-circle"></i></div>
									<div class="col-lg-8">
										<h2 style="color: #FFFFFF">Pengguna</h2>
										<div class="hero_box_link">Lihat Detail</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>

				<div class="col-lg-3 hero_box_col">
					<a href="<?php echo site_url('admin-galeri'); ?>">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<div class="hero_box_content">
								<div class="row">
									<div class="col-lg-4"><i class="far fa-image"></i></div>
									<div class="col-lg-8">
										<h2 style="color: #FFFFFF">Galeri</h2>
										<div class="hero_box_link">Lihat Detail</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>

			<div class="row general_row justify-content-md-center">
				<div class="col-lg-3 hero_box_col">
					<a href="<?php echo site_url('admin-program'); ?>">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<div class="hero_box_content">
								<div class="row">
									<div class="col-lg-4"><i class="far fa-list-alt"></i></div>
									<div class="col-lg-8">
										<h2 style="color: #FFFFFF">Program</h2>
										<div class="hero_box_link">Lihat Detail</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>

				<div class="col-lg-3 hero_box_col">
					<a href="<?php echo site_url('admin-berita'); ?>">
						<div class="hero_box d-flex flex-row align-items-center justify-content-start">
							<div class="hero_box_content">
								<div class="row">
									<div class="col-lg-4"><i class="far fa-newspaper"></i></div>
									<div class="col-lg-8">
										<h2 style="color: #FFFFFF">Berita</h2>
										<div class="hero_box_link">Lihat Detail</div>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>
