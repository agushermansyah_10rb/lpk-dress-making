<?php $this->load->view('template/header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/styles/news_responsive.css'); ?>">
<div class="super_container">
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Berita LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Berita Kami</h1>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center">
				<div class="col-lg-8">
					<div class="news_posts">
						<?php 
							if ($data) {
								foreach ($data as $v) {
						?>
							<!-- News Post -->
							<div class="news_post">
								<?php 
									if ($v->gambar) {
								?>
									<img src="<?php echo base_url("asset/images/berita/".$v->gambar); ?>" alt="Dress Making" class="img-fluid mx-auto d-block">
								<?php 
									}
								?>
								<div class="news_post_top d-flex flex-column flex-sm-row">
									<div class="news_post_date_container">
										<div class="news_post_date d-flex flex-column align-items-center justify-content-center">
											<div>
												<?php
													$dt = new DateTime($v->dibuat_pada); 
													echo $dt->format('d'); 
												?>	
											</div>
											<div>
												<?php
													$dt = new DateTime($v->dibuat_pada); 
													echo $dt->format('M'); 
												?>	
											</div>
										</div>
									</div>
									<div class="news_post_title_container">
										<div class="news_post_title">
											<a href="<?php echo site_url('berita-detail/'.$v->berita_id);?>"><?php echo $v->judul; ?></a>
										</div>
										<div class="news_post_meta">
											<span class="news_post_author"><a href="#">Oleh <?php echo $v->penulis; ?></a></span>
										</div>
									</div>
								</div>
								<div class="news_post_text">
									<?php 
										if (preg_match('%(<p[^>]*>.*?</p>)%i', $v->teks, $regs)) {
										    echo preg_replace("/<p>/", "<p style='text-align: justify'>", $regs[1]);
										} else {
										    echo "Lihat detail berita dengan meng-klik lanjutkan!";
										}
									?>
								</div>
								<div class="news_post_button text-center trans_200">
									<a href="<?php echo site_url('berita-detail/'.$v->berita_id);?>">Lanjutkan</a>
								</div>
							</div>
						<?php 
								}
							} 
							else {
				  				echo "<h2 style='text-align:center;'> Tidak ada data </h2>";
				  		}
						?>
					</div>
				</div>
			</div>

			<div class="row general_row justify-content-md-center">
				<div class="col-lg-8">
					<?php 
						if ($total != 0) {
					?>
					    <ul class="pagination justify-content-center">
					        <?php
						        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 1;
						        $limit = 5;
						       	$start = ($page - 1) * $limit;
						        
						        if($page == 1) {
					        ?>
						        	<li class="page-item disabled"><a href="#" class="page-link">Pertama</a></li>
						          	<li class="page-item disabled"><a href="#" class="page-link">&laquo;</a></li>
					        <?php
						        } 
						        else { 
						        	// Jika page bukan page ke 1
						        	$link_prev = ($page > 1) ? $page - 1 : 1;
					        ?>
					        	<li><a href="<?php echo site_url('berita/1'); ?>" class="page-link">Pertama</a></li>
					          	<li><a href="<?php echo site_url('berita/'.$link_prev); ?>" class="page-link">&laquo;</a></li>
					        <?php
					        	}
					        ?>
					        
					        <?php
					        	$jumlah_page = ceil($total / $limit); // Hitung jumlah halamannya
					        	$jumlah_number = ($jumlah_page > 10) ? 10 : $jumlah_page; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
					        	$start_number = ($page > $jumlah_number) ? ($page - ($jumlah_number - 1)) : 1; // Untuk awal link number
					        	$end_number = ($page <= $jumlah_number) ? $jumlah_number : ($start_number + ($jumlah_number - 1)); // Untuk akhir link number
					        
						        for($i = $start_number; $i <= $end_number; $i++) {
						  			$link_active = ($page == $i) ? 'active' : '';
					        ?>
					        		<li class="page-item <?php echo $link_active; ?>">
					        			<a href="<?php echo site_url('berita/'.$i); ?>" class="page-link"><?php echo $i; ?></a>
					        		</li>
					        <?php
					        	}
					        ?>
					        
					        <?php
						        if($page == $jumlah_page) { // Jika page terakhir
					        ?>
							        <li class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>
							        <li class="page-item disabled"><a href="#" class="page-link">Terakhir</a></li>
					        <?php
						        }
						        else { // Jika Bukan page terakhir
						          	$link_next = ($page < $jumlah_page) ? ($page) + 1 : $jumlah_page;
					        ?>
						         	<li><a href="<?php echo site_url('berita/'.$link_next); ?>" class="page-link">&raquo;</a></li>
						          	<li><a href="<?php echo site_url('berita/'.$jumlah_page); ?>" class="page-link">Terakhir</a></li>
					        <?php
					        	}
					        ?>
					    </ul>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>