<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Program Kursus LKP/LPK Fadylah</h1>
			</div>
		</div>
	</div>

	<!-- Galeri -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Program Kursus Kami</h1>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center">
				<?php
					if ($data) {
						$i = 1;
						foreach($data as $v) {
				?>
					<div class="col-lg-4 general_item" style="text-align: center;">
						<div class="card">
							<?php 
								if ($v->gambar) {
							?>

						    	<img class="card-img-top" src="<?php echo base_url("asset/images/program/$v->gambar"); ?>" alt="Card image" style="width:100%">
						    <?php
						    	}
						    ?>
						    <div class="card-body" style="padding: 25px">
						    	<h2 class="card-title" style="margin-top: 20px"><?php echo $v->judul; ?></h2>
						      	<p class="card-text" style="margin-top: 10px"><?php echo $v->keterangan; ?></p>
						      	<a href="<?php echo site_url('daftar'); ?>" class="btn general_submit_button" style="padding-top: 12px">Daftar</a>
						    </div>
						</div>
					</div>
				<?php 
						$i++;
						}
					}
					else {
						echo "<h2 style='text-align:center;'> Tidak ada data </h2>";
					}
				?>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>

