<?php $this->load->view('template/header'); ?>
<script src="<?php echo base_url("asset/js/sweetalert/sweetalert.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/alert/alert.js"); ?>"></script>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Pendaftaran LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<!-- Response status -->
    <?php 
        if ($_GET) {
            if (isset($_GET['status'])) {
                $status = $_GET['status']; 
                $error = '';
                
                if (isset($_GET['error'])) {
                	$error = $_GET['error'];
                }

                $pesan = 'Kesalah Tidak Diketahui';
                if ($error == '1') {
                	$pesan = "NIK Tidak Benar";
                }
                else if ($error == '2') {
                	$pesan = "Email Tidak Benar";
                }
                else if ($error == '3') {
                	$pesan = "No Telp Tidak Benar";
                }
                else if ($error == '4') {
                	$pesan = "NIK Sudah Terdaftar";
                }
                else if ($error == '5') {
                	$pesan = "Email Sudah Terdaftar";
                }
                else if ($error == '6') {
                	$pesan = "No Telp Sudah Terdaftar";
                }
                else if ($error == '7') {
                	$pesan = "Format Gambar Tidak Sesuai Ketentuan (.jpeg/.jpg/.png)";
                }
                else if ($error == '8') {
                	$pesan = "Ukuran Gambar Terlalu Besar (Maksimal 500Kb)";
                }
                else if ($error == '9') {
                	$pesan = "Gagal Mengupload Gambar";
                }

                if ($status == 'sukses') {
                    echo "<script type='text/javascript'>success('Data Telah Tersimpan', 'daftar', '', 'false');</script>";
                } 
                else if ($status == 'gagal') {
                   	echo "<script type='text/javascript'>failed('$pesan', 'daftar');</script>";
                }
            }
        }
    ?>

	<!-- Identitas Lembaga -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Form Pendaftaran</h1>
						<p>Silakan isi data dengan benar!</p>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
				<div class="col-lg-6">
					<form action="<?php echo site_url('pendaftaran/daftar'); ?>" method="POST" enctype="multipart/form-data">
						<div class="form-group">
					   		<label for="nik">* NIK : </label>
					    	<input type="text" class="form-control general_font_input" placeholder="Masukkan NIK" name="nik" id="nik" required="true">
					  	</div>
						<div class="form-group">
					   		<label for="nama">* Nama Lengkap : </label>
					    	<input type="text" class="form-control general_font_input" placeholder="Masukkan Nama Lengkap" name="nama_lengkap" id="nama" required="true">
					  	</div>
					  	<div class="form-group">
					   		<label for="email">* Email : </label>
					    	<input type="text" class="form-control general_font_input" placeholder="Masukkan Email" name="email" id="email" required="true">
					  	</div>
					  	<div class="form-group">
					   		<label for="telp">* No. Telp : </label>
					    	<input type="text" class="form-control general_font_input" placeholder="Masukkan Nomor Telpon" name="no_telp" id="telp" required="true">
					  	</div>
					  	<div class="form-group">
					   		<label for="jenis_kelamin">* Jenis Kelamin : </label>
					    	<select class="form-control general_font_input" name="jenis_kelamin" id="jenis_kelamin" required="true">
					    		<option value="">Pilih Jenis Kelamin</option>
					    		<option value="L">Pria</option>
					    		<option value="P">Wanita</option>
					    	</select>
					  	</div>
					  	<div class="form-group">
					   		<label for="tempat_lahir">* Tempat Lahir : </label>
					    	<input type="text" class="form-control general_font_input" placeholder="Masukkan Tempat Lahir" name="tempat_lahir" id="tempat_lahir" required="true">
					  	</div>
						<div class="form-group">
							<label for="Tanggal_lahir">* Tanggal Lahir: </label>
							<div class="input-group date">
						    	<div class="input-group-addon">
						           <span class="far fa-calendar-alt"></span>
						       	</div>
						       	<input type="text" class="form-control date-picker general_font_input" placeholder="Masukkan Tanggal Lahir" name="tanggal_lahir" id="tanggal_lahir" required="true">
						   	</div>
						</div>
					  	<div class="form-group">
					   		<label for="nama_ibu">* Nama Ibu : </label>
					    	<input type="text" class="form-control general_font_input" placeholder="Masukkan Nama Ibu" name="nama_ibu" id="nama_ibu" required="true">
					  	</div>
					    <div class="form-group">
					   		<label for="pendidikan">* Pilih Pendidikan : </label>
					    	<select class="form-control general_font_input" name="pendidikan" id="pendidikan" required="true">
					    		<option value="">Pilih Pendidikan</option>
					    		<option value="sd">SD Sederajat</option>
					    		<option value="smp">SMP Sederajat</option>
					    		<option value="sma">SMA Sederajat</option>
					    		<option value="d1">D1</option>
					    		<option value="d2">D2</option>
					    		<option value="d3">D3</option>
					    		<option value="s1">S1</option>
					    		<option value="s2">S2</option>
					    		<option value="s3">S3</option>
					    	</select>
					    </div>
					  	<div class="form-group">
					   		<label for="jenis_kursus">* Jenis Kursus : </label>
					    	<select class="form-control general_font_input" name="jenis_kursus" id="jenis_kursus" required="true">
					    		<option value="">Pilih Jenis Kursus</option>
					    		<option value="busana industri">Busana Industri</option>
					    		<option value="tata busana">Tata Busana</option>
					    	</select>
					    </div>
					    <div class="form-group">
					   		<label for="waktu">* Waktu Kursus : </label>
					    	<select class="form-control general_font_input" name="waktu_kursus" id="waktu_kursus" required="true">
					    		<option value="">Pilih Waktu Kursus</option>
					    		<option value="reguler">Reguler (Senin - Jumat)</option>
					    		<option value="khusus">Khusus (Sabtu)</option>
					    	</select>
					    </div>
					    <div class="form-group">
					   		<label for="foto">* Foto (Opsional - .jpeg/.jpg/.png - maks 500Kb) : </label>
					    	<input type="file" class="form-control general_font_input" name="image" id="foto">
					  	</div>
					    <div class="form-group">
					   		<label for="provinsi">* Provinsi : </label>
					    	<select class="form-control general_font_input" name="provinsi" id="provinsi" required="true" onchange="ajaxKota('<?php echo base_url(); ?>', this.value)">
					    		<option value="">Pilih Provinsi</option>
					    		<?php 
					    			foreach ($provinsi as $prov) {
					    		?>
					    			<option value="<?php echo $prov->id_prov.'|'.$prov->nama; ?>"><?php echo $prov->nama; ?></option>
					    		<?php
					    			}
					    		?>
					    	</select>
					    </div>
					    <div class="form-group">
					   		<label for="kabupaten">* Kabupaten : </label>
					    	<select class="form-control general_font_input" name="kabupaten" id="kabupaten" required="true" onchange="ajaxKecamatan('<?php echo base_url(); ?>', this.value)">
					    		<option value="">Pilih Kabupaten</option>
					    	</select>
					    </div>
					    <div class="form-group">
					   		<label for="kecamatan">* Kecamatan : </label>
					    	<select class="form-control general_font_input" name="kecamatan" id="kecamatan" required="true" onchange="ajaxKelurahan('<?php echo base_url(); ?>', this.value)">
					    		<option value="">Pilih Kecamatan</option>
					    	</select>
					    </div>
					    <div class="form-group">
					   		<label for="kelurahan">* Kelurahan : </label>
					    	<select class="form-control general_font_input" name="kelurahan" id="kelurahan">
					    		<option value="">Pilih Kelurahan</option>
					    	</select>
					    </div>
					    <div class="form-group">
					   		<label for="alamat">* Alamat Lengkap: </label>
					    	<textarea class="form-control general_font_input" rows="4" name="alamat" id="alamat" required="true"></textarea>
					  	</div>
					  	<input type="submit" name="submit" value="Daftar" class="general_submit_button">
					</form>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>
<script src="<?php echo base_url("asset/js/daerah.js"); ?>"></script>

<script type="text/javascript">
	$(function(){
		$(".date-picker").datepicker({
			endDate: '-0m',
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
	});
</script>