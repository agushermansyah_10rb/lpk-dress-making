<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Kuis Online LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<!-- Kuis -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Kuis Pertemuan Ke <?php echo ($video) ? $video->tingkat : 0; ?></h1>
						<p><?php echo ($video) ? $video->judul : ''; ?></p>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
				<div class="col-lg-9">
					<?php 
						if ($data) {
					?>
					<form action="<?php echo site_url('jawab_kuis'); ?>" method="POST">
					  	<table class="table table-borderless">
					  		<?php 
					  			$i = 1;
					  			foreach ($data as $v) {
					  		?>
					  			<input type="hidden" name="id[]" value="<?php echo $v->kpj_id; ?>">
								<input type="hidden" name="jumlah" value="<?php echo $total; ?>">
								<input type="hidden" name="kuis_id" value="<?php echo $v->kuis_id; ?>">
								<input type="hidden" name="video_id" value="<?php echo $v->video_id; ?>">
							    <thead>
								    <tr>
								        <th style="width: 5%"><?php echo $i; ?></th>
								        <th><?php echo $v->pertanyaan; ?></th>
								    </tr>
							    </thead>
							    <tbody>
							      	<tr>
							        	<td></td>
							        	<td><input name="pilihan[<?php echo $v->kpj_id; ?>]" type="radio" value="A"> <?php echo $v->a; ?></td>
							      	</tr>
							      	<tr>
							        	<td></td>
							        	<td><input name="pilihan[<?php echo $v->kpj_id; ?>]" type="radio" value="B"> <?php echo $v->b; ?></td>
							      	</tr>
							      	<tr>
							        	<td></td>
							        	<td><input name="pilihan[<?php echo $v->kpj_id; ?>]" type="radio" value="C"> <?php echo $v->c; ?></td>
							      	</tr>
							      	<tr>
							        	<td></td>
							        	<td><input name="pilihan[<?php echo $v->kpj_id; ?>]" type="radio" value="D"> <?php echo $v->d; ?></td>
							      	</tr>
							    </tbody>
							<?php 
								$i++;
								}
							?>
							<tr>
								<td></td>
								<td>
									<input class="btn btn-primary float-right general_submit_button2" type="submit" name="submit" value="Jawab" onclick="return confirm('Apakah Anda yakin dengan jawaban Anda?')" style="background: margin-top: 10px;">
								</td>
							</tr>
					  	</table>
					</form>
				  	<?php
				  		}
				  		else {
				  			echo "<h2 style='text-align:center;'> Tidak ada data </h2>";
				  		}
				  	?>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>