<?php $this->load->view('template/header'); ?>
<script src="<?php echo base_url("asset/js/pendaftaran.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/sweetalert/sweetalert.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/alert/alert.js"); ?>"></script>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Detail Peserta</h1>
			</div>
		</div>
	</div>

	<!-- Response status -->
    <?php 
        if ($_GET) {
            if (isset($_GET['status'])) {
                $status = $_GET['status']; 

                $peserta_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : '';
                $url = site_url('detailp/'.$peserta_id);
                $pesan = 'Data Gagal Dihapus';

                if ($status == 'sukses') {
                    echo "<script type='text/javascript'>success('Data Telah Terhapus', '$url', '', 'false');</script>";
                } 
                else if ($status == 'gagal') {
                   	echo "<script type='text/javascript'>failed('$pesan', '$url');</script>";
                }
            }
        }
    ?>

	<!-- Identitas Lembaga -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<!-- <h1></h1> -->
						<!-- <p></p> -->
					</div>
				</div>
			</div>
			
			<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
				<div class="col-lg-8">
					<?php 
						if ($data) {
							$v = $data;
					?>
					<div class="row justify-content-md-center">
						<div class="col-lg-12 d-flex justify-content-center">
							<div class="wrapper-profil d-flex justify-content-center">
								<?php 
									if ($v->foto_profil) {
								?>
									<img src="<?php echo base_url('asset/images/pendaftaran/'.$v->foto_profil);?>">
								<?php
									}
									else {
								?>
									<img src="<?php echo base_url('asset/images/pendaftaran/user.png');?>" style="width: 125px; height: 125px; margin-top: 20px">
								<?php 
									}
								?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<h2 style="margin-bottom: -2px"><?php echo $v->nama_lengkap; ?></h2>
							<?php 
								if ($v->status == 'proses') {
									echo "<p>Peserta Sedang Dalam Proses</p>";
								}
								else if ($v->status == 'disetujui') {
									echo "<p>Peserta Telah Disetujui</p>";
								}
								else if ($v->status == 'ditolak') {
									echo "<p>Peserta Telah Ditolak</p>";
								}
							?>
						</div>
					</div>
					<br>
				  	<table class="table table-bordered">
					    <tbody>
					      	<tr>
					        	<td>NIK</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->nik; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Nama Lengkap</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->nama_lengkap; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Email</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->email; ?></td>
					      	</tr>
					      	<tr>
					        	<td>No Telp</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->no_telp; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Jenis Kelamin</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->jenis_kelamin; ?></td>
					      	</tr>		
					      	<tr>
					        	<td>TTL</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->tempat_lahir.', '.$v->tanggal_lahir; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Alamat Lengkap</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->alamat; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Provinsi</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->provinsi; ?></td>
					      	</tr>
					      		<tr>
					        	<td>Kabupaten</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->kabupaten; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Kecamatan</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->kecamatan; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Kelurahan</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->kelurahan; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Pendidikan</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->pendidikan; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Nama Ibu</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->nama_ibu; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Jenis Kursus</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->jenis_kursus; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Watu Kursus</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->waktu; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Status</td>
					        	<td class="text-center">:</td>
					        	<td>
					        		<?php 
					        			$badge = "secondary";
					        			$status = $v->status;
					        			if ($status == 'disetujui') {
					        				$badge = "success";
					        			}
					        			else if ($status == 'ditolak') {
					        				$badge = "danger";
					        			}
					        		?>
					        		<span class="badge badge-<?php echo $badge; ?>"><?php echo $status; ?>
					        	</td>
					      	</tr>
					      	<tr>
					      		<?php 
				        			$badge = "success";
				        			$ket = "Ya";
				        			$lulus = $v->lulus;
				        			if ($lulus == '0') {
				        				$badge = "danger";
				        				$ket = "belum";
				        			}
				        		?>
					        	<td>Lulus</td>
					        	<td class="text-center">:</td>
					        	<td><span class="badge badge-<?php echo $badge; ?>"><?php echo $ket; ?></td>
					      	</tr>
					      	<tr>
					        	<td>Tanggal Daftar</td>
					        	<td class="text-center">:</td>
					        	<td><?php echo $v->dibuat_pada; ?></td>
					      	</tr>
					    </tbody>
				  	</table>
				  	<button class="btn btn-primary float-right" onclick="detailPeserta('<?php echo site_url('edit-peserta/'.$v->peserta_id); ?>')"><i class="fas fa-edit"></i> Edit</button>
				  	<button class="btn btn-danger float-right" onclick="ask_del('<?php echo site_url('admin/hapus_peserta/'.$v->peserta_id); ?>')" style="margin-right: 5px"><i class="fas fa-trash"></i> Hapus</button>
				  	<?php
				  		}
				  		else {
				  			echo "<h2 style='text-align:center;'> Tidak ada data </h2>";
				  		}
				  	?>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>
