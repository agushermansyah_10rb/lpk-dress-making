<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Galeri LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<!-- Galeri -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Galeri Kami</h1>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center">
				<div id="jumlahGambar"></div>
				<?php
					if ($data) {
						$i = 1;
						foreach($data as $v) {
				?>
					<div class="col-lg-4 general_item" style="text-align: center;">
						<div class="x">
							<img id="<?php echo 'gambar'.$i; ?>" class="galeriImg" src="<?php echo base_url("asset/images/galeri/$v->gambar"); ?>" alt="<?php echo $v->keterangan; ?>" width="325px" height="225px">
						</div>
					</div>
				<?php 
						$i++;
						}
					}
					else {
						echo "<h2 style='text-align:center;'> Tidak ada data </h2>";
					}
				?>
				<!-- The Modal -->
				<div id="myModal" class="modal">
				  	<span class="close" style="color: #FFFFFF">x</span>
				  		<img class="modal-content" id="pict">
				  	<div id="caption"></div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center">
				<div class="col-lg-6">
					<?php 
						if ($total != 0) {
					?>
					    <ul class="pagination justify-content-center">
					        <?php
						        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 1;
						        $limit = 9;
						       	$start = ($page - 1) * $limit;
						        
						        if($page == 1) {
					        ?>
						        	<li class="page-item disabled"><a href="#" class="page-link">Pertama</a></li>
						          	<li class="page-item disabled"><a href="#" class="page-link">&laquo;</a></li>
					        <?php
						        } 
						        else { 
						        	// Jika page bukan page ke 1
						        	$link_prev = ($page > 1) ? $page - 1 : 1;
					        ?>
					        	<li><a href="<?php echo site_url('galeri/1'); ?>" class="page-link">Pertama</a></li>
					          	<li><a href="<?php echo site_url('galeri/'.$link_prev); ?>" class="page-link">&laquo;</a></li>
					        <?php
					        	}
					        ?>
					        
					        <?php
					        	$jumlah_page = ceil($total / $limit); // Hitung jumlah halamannya
					        	$jumlah_number = ($jumlah_page > 10) ? 10 : $jumlah_page; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
					        	$start_number = ($page > $jumlah_number) ? ($page - ($jumlah_number - 1)) : 1; // Untuk awal link number
					        	$end_number = ($page <= $jumlah_number) ? $jumlah_number : ($start_number + ($jumlah_number - 1)); // Untuk akhir link number
					        
						        for($i = $start_number; $i <= $end_number; $i++) {
						  			$link_active = ($page == $i) ? 'active' : '';
					        ?>
					        		<li class="page-item <?php echo $link_active; ?>">
					        			<a href="<?php echo site_url('galeri/'.$i); ?>" class="page-link"><?php echo $i; ?></a>
					        		</li>
					        <?php
					        	}
					        ?>
					        
					        <?php
						        if($page == $jumlah_page) { // Jika page terakhir
					        ?>
							        <li class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>
							        <li class="page-item disabled"><a href="#" class="page-link">Terakhir</a></li>
					        <?php
						        }
						        else { // Jika Bukan page terakhir
						          	$link_next = ($page < $jumlah_page) ? ($page) + 1 : $jumlah_page;
					        ?>
						         	<li><a href="<?php echo site_url('galeri/'.$link_next); ?>" class="page-link">&raquo;</a></li>
						          	<li><a href="<?php echo site_url('galeri/'.$jumlah_page); ?>" class="page-link">Terakhir</a></li>
					        <?php
					        	}
					        ?>
					    </ul>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>

<script>
	var modal = document.getElementById('myModal');
	var i;
	var images = document.getElementsByClassName('galeriImg');

	for (i=1; i<=images.length; i++) {
		var img = document.getElementById("gambar" + i);
		var modalImg = document.getElementById("pict");
		var captionText = document.getElementById("caption");

		img.onclick = function(){
		  	modal.style.display = "block";
		  	modalImg.src = this.src;
		  	captionText.innerHTML = this.alt;
		}
	}

	var span = document.getElementsByClassName("close")[0];
	span.onclick = function() { 
		modal.style.display = "none";
	}
</script>