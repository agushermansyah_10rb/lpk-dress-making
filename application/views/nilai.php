<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Nilai Kuis</h1>
			</div>
		</div>
	</div>

	<!-- Nilai -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Data Nilai</h1>
						<p>Hasil penilaian peserta selama kursus</p>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
				<div class="col-lg-10">
				  	 <div class="card" style="background: none">
                        <div class="card-body">
                        	<div class="table-responsive" style="margin-top: 15px;">
								<table id="zero_config" class="table table-bordered">
							  		<thead>
									    <tr>
									        <th>No</th>
									        <th>Nama</th>
									        <th>Kuis</th>
									        <th>Jumlah Soal</th>
									        <th>Benar</th>
									        <th>Salah</th>
									        <th>Skor</th>
									        <th>Tanggal</th>
									    </tr>
									</thead>

							  		<?php 
							  			if ($data) {
								  			$i = 1;
								  			foreach ($data as $v) {
							  		?>
									    <tbody>
									      	<tr>
									        	<td><?php echo $i; ?></td>
									        	<td><?php echo $v->nama_lengkap; ?></td>
									        	<td><?php echo $v->nama; ?></td>
									        	<td><?php echo $v->jumlah_soal; ?></td>
									        	<td><?php echo $v->jumlah_benar; ?></td>
									        	<td><?php echo $v->jumlah_salah; ?></td>
									        	<td><?php echo $v->skor; ?></td>
									        	<td><?php echo $v->dibuat_pada; ?></td>
									      	</tr>
									    </tbody>
									<?php 
										$i++;
										}
								  	}
								  	else {
								  	?>
								  		<tbody>
									      	<tr>
									        	<td colspan="8" style="text-align: center;">Tidak Ada Data</td>
									        </tr>
									    </tbody>
								  	<?php
								  		}
								  	?>
							  	</table>
						  		<?php echo 'Total : '.$total.' Item'; ?>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center">
				<div class="col-lg-6">
					<?php 
						if ($total != 0) {
					?>
					    <ul class="pagination justify-content-center">
					        <?php
						        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 1;
						        $limit = 10;
						       	$start = ($page - 1) * $limit;
						       	$q = isset($pid) ? "?pid=".$pid : "";
						        
						        if($page == 1) {
					        ?>
						        	<li class="page-item disabled"><a href="#" class="page-link">Pertama</a></li>
						          	<li class="page-item disabled"><a href="#" class="page-link">&laquo;</a></li>
					        <?php
						        } 
						        else { 
						        	// Jika page bukan page ke 1
						        	$link_prev = ($page > 1) ? $page - 1 : 1;
					        ?>
					        	<li><a href="<?php echo site_url('nilai/1'.$q); ?>" class="page-link">Pertama</a></li>
					          	<li><a href="<?php echo site_url('nilai/'.$link_prev.$q); ?>" class="page-link">&laquo;</a></li>
					        <?php
					        	}
					        ?>
					        
					        <?php
					        	$jumlah_page = ceil($total / $limit); // Hitung jumlah halamannya
					        	$jumlah_number = ($jumlah_page > 10) ? 10 : $jumlah_page; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
					        	$start_number = ($page > $jumlah_number) ? ($page - ($jumlah_number - 1)) : 1; // Untuk awal link number
					        	$end_number = ($page <= $jumlah_number) ? $jumlah_number : ($start_number + ($jumlah_number - 1)); // Untuk akhir link number
					        
						        for($i = $start_number; $i <= $end_number; $i++) {
						  			$link_active = ($page == $i) ? 'active' : '';
					        ?>
					        		<li class="page-item <?php echo $link_active; ?>">
					        			<a href="<?php echo site_url('nilai/'.$i.$q); ?>" class="page-link"><?php echo $i; ?></a>
					        		</li>
					        <?php
					        	}
					        ?>
					        
					        <?php
						        if($page == $jumlah_page) { // Jika page terakhir
					        ?>
							        <li class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>
							        <li class="page-item disabled"><a href="#" class="page-link">Terakhir</a></li>
					        <?php
						        }
						        else { // Jika Bukan page terakhir
						          	$link_next = ($page < $jumlah_page) ? ($page) + 1 : $jumlah_page;
					        ?>
						         	<li><a href="<?php echo site_url('nilai/'.$link_next.$q); ?>" class="page-link">&raquo;</a></li>
						          	<li><a href="<?php echo site_url('nilai/'.$jumlah_page.$q); ?>" class="page-link">Terakhir</a></li>
					        <?php
					        	}
					        ?>
					    </ul>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>