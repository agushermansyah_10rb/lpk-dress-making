<style type="text/css">
	.skor {
		color: red;
	}
	.skor h1 {
		color: #000000;
	}
	.skor h2 {
		color: #FFFFFF;
		margin-top: 5px;
	}
	.skor h3 {
		color: #000000;
		margin-top: 15px;
	}
	.skor p {
		color: #000000;
	}
	.badge {
		width: 150px;
		background: #003e4e;
		margin-top: 5px;
	}
	.skor span {
		color: #000000;
	}
	.bg {
		background: #ebebeb;
		padding: 35px;
		border-radius: 7px 7px 5px 5px;
	}
	.bg2 {
		background: #003e4e;
		padding: 15px;
		color: #FFFFFF;
		border-radius: 0px 0px 7px 7px;
	}
	.bg2 a {
		color: #FFFFFF;
	}
	.bg2 a:hover {
		color: #FFFFFF;
	}

</style>

<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<!-- Kuis -->
	<div class="general page_section">
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-lg-4 bg">
					<div class="skor text-center">
						<h1 style="font-size: 30px">Skor Anda</h1>
						<?php 
							if ($data) {
						?>
							<div class="badge">
								<h2><?php echo $data->skor; ?>/100</h2>
							</div>
							<br>
							<br>
							<span><b>Keterangan :</b></span>
							<br>
							<span>Benar: <b><?php echo $data->jumlah_benar; ?></b></span>
							<span>Salah: <b><?php echo $data->jumlah_salah; ?></b></span>
						<?php 
							} else {
						?>
							<div class="badge">
								<h2>0/0</h2>
							</div>
							<br>
							<br>
							<span><b>Keterangan :</b></span>
							<br>
							<span>Benar: <b>0</b></span>
							<span>Salah: <b>0</b></span>
						<?php
							}
						?>
						<h3>Terimakasih telah mengikuti kuis!</h3>
					</div>
				</div>
			</div>
			<div class="row justify-content-md-center">
				<div class="col-lg-4 bg2">
					<div class="kembali text-center">
						<a href="<?php echo site_url('kursus'); ?>"><b>Kembali</b></a>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/footer'); ?>