<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Kepemilikan LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<!-- Kepemilikan -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Kepemilikan Lembaga</h1>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php 
		if ($prestasi) {
	?>
	<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
		<div class="col-lg-8">
		  	<table class="table table-bordered">
		  		<thead>
		  			<tr>
		  				<th colspan="4" style="text-align: center; background: #606060; color:#FFFFFF">PRESTASI LEMBAGA</th>
		  			</tr>
				    <tr>
				        <th>No</th>
				        <th>Nama</th>
				        <th>Tingkat</th>
				        <th>Tahun</th>
				    </tr>
				</thead>
		  		<?php 
		  			$i = 1;
		  			foreach ($prestasi as $v) {
		  		?>
				    <tbody>
				      	<tr>
				        	<td><?php echo $i; ?></td>
				        	<td><?php echo $v->nama; ?></td>
				        	<td><?php echo $v->tingkat; ?></td>
				        	<td><?php echo $v->tahun; ?></td>
				      	</tr>
				    </tbody>
				<?php 
					$i++;
					}
				?>
		  	</table>
		</div>
	</div>
	<?php
		}
	?>

	<?php 
		if ($sarana) {
	?>
	<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
		<div class="col-lg-8">
		  	<table class="table table-bordered">
		  		<thead>
		  			<tr>
		  				<th colspan="4" style="text-align: center; background: #606060; color:#FFFFFF">SARANA DAN PRASARANA</th>
		  			</tr>
				    <tr>
				        <th>No</th>
				        <th>Nama</th>
				        <th>Tingkat</th>
				        <th>Tahun</th>
				    </tr>
				</thead>
		  		<?php 
		  			$i = 1;
		  			foreach ($sarana as $v) {
		  		?>
				    <tbody>
				      	<tr>
				        	<td><?php echo $i; ?></td>
				        	<td><?php echo $v->nama; ?></td>
				        	<td><?php echo $v->jumlah; ?></td>
				        	<td><?php echo $v->kondisi; ?></td>
				      	</tr>
				    </tbody>
				<?php 
					$i++;
					}
				?>
		  	</table>
		</div>
	</div>
	<?php
		}
	?>

	<?php 
		if ($tenaga) {
	?>
	<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
		<div class="col-lg-8">
		  	<table class="table table-bordered">
		  		<thead>
		  			<tr>
		  				<th colspan="4" style="text-align: center; background: #606060; color:#FFFFFF">TENAGA PENDIDIK</th>
		  			</tr>
				    <tr>
				        <th>No</th>
				        <th>Nama</th>
				        <th>Jumlah</th>
				        <th>Keterangan</th>
				    </tr>
				</thead>
		  		<?php 
		  			$i = 1;
		  			foreach ($tenaga as $v) {
		  		?>
				    <tbody>
				      	<tr>
				        	<td><?php echo $i; ?></td>
				        	<td><?php echo $v->nama; ?></td>
				        	<td><?php echo $v->jumlah; ?></td>
				        	<td><?php echo $v->keterangan; ?></td>
				      	</tr>
				    </tbody>
				<?php 
					$i++;
					}
				?>
		  	</table>
		</div>
	</div>
	<?php
		}
	?>
	<br>
	<br>
	<br>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>