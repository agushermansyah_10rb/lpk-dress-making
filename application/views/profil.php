<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Profil LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<!-- Identitas Lembaga -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Identitas Lembaga</h1>
					</div>
				</div>
			</div>
			<div class="row general_row">
				<div class="col-lg-6 general_item">
					<div class="card" style="text-align: center; padding: 20px; background: none">
						<div class="card_img">
							<img class="card-img-top trans_200" src="<?php echo base_url("asset/images/lpk/logo-big.jpg"); ?>" style="width:300px; height:300px; border-radius: 50%;">
						</div>
						<h1 style="color: #000; font-size: 25px; padding-top:30px;">LKP/LPK Dress Making</h1>
					</div>
				</div>

				<div class="col-lg-6 general_item">
					<p>
						<b>Nama lembaga</b> : LKP/LPK Dress Making <br>
						<b>NPSN</b> : K5662097 <br>
						<b>NILEK</b> : 02205.4.1.00.19.35 <br>
						<b>VIN</b> : 2004327701 <br>
						<b>Nama pimpinan</b> : Dewi S Fujiwara, S.Pd.I <br>
						<b>Hasil Akreditasi LKP</b> : B (BAN-PNF 2018)<br>
						<b>Hasil Akreditasi LPK</b> : Terakeditasi (LA-LPK 2019) <br>
						<b>Telepon</b> : 081223246677 / 085222575577 <br>
						<b>Tahun Berdiri</b> : 13 Agustus 1997 <br>
						<b>Alamat</b> : Jl. Sentral No. 28 RT 01 RW 12 Kel. Cibabat Kec. Cimahi Utara Kota Cimahi Provinsi Jawa Barat, 40513<br>
						<b>Tahun berdiri</b> : 25 September 2004 <br>
						<b>Izin Operasional Disnaker</b> : 560/Kep.1591/Disnaker/2019 <br>
						<b>Izin Operasional Disdik</b> : 421.9/Kep.0946-disdik/2017<br>
						<b>Izin Operasional NIB</b> : 0220207612245<br>
						<b>Akta notaris</b> : No. 16 (28 Oktober 2009) Tatti Muktiati Hidayat, S.H <br>
						<b>SK TUK LSK TB</b> : KEP.069/LSK-TB/X/2010 <br>
					</p>
				</div>
			</div>
		</div>
	</div>

	<!-- Visi Misi -->
	<div class="register">
		<div class="container-fluid">
			<div class="row row-eq-height">
				<div class="col-lg-6 nopadding">
					<div class="search_section d-flex flex-column align-items-center justify-content-center" style="background: #003e4e">
						<h1 class="search_title" style="color:#fff; padding-bottom: 15px;">Visi</h1>
						<p style="color:#fff; font-weight: 300; line-height: 20px; text-align: justify;">
							Menjadikan lembaga kursus dan pelatihan yang terpercaya dan berkualitas secara professional mendapatkan pencitraan (pengakuan) ditingkat nasional dan internasional dalam rangka menciptakan sumber daya manusia (SDM) yang cerdas, mandiri, berkepribadian, beriman, dan bertaqwa kepada Tuhan Yang Maha Esa.
						</p>
					</div>
				</div>

				<div class="col-lg-6 nopadding">
					<div class="search_section d-flex flex-column align-items-center justify-content-center">
						<h1 class="search_title">Misi</h1>
						<p>
							<ol style="color: #000000; font-weight: 300; text-align: justify;">
								<li>Menciptakan budaya dan iklim kerja yang kondusif untuk mewujudkan SDM yang interaktif, berinisiatif, menyenangkan, menantang, dan memotivasi.</li>
								<li>Mengoptimalkan program yang telah dilaksanakan dalam rangka meningkatkan profesionalisme lembaga kursus dan pelatihan.</li>
								<li>Menampilkan keunggulan (inovasi) dalam pengelolaan lembaga kursus dan pelatihan yang akan mewujudkan pencitraan (pengakuan) ditingkat nasional dan di internasional.</li>
								<li>Memiliki jaringan di tingkat nasional dan internasional menjalin komukasi dan hubungan kolegia dengan organisasi-organisasi pemerintah dan swasta (industri) mitra yang terkait.</li>
								<li>Memberikan layanan bimbingan pelatihan dan kursus yang bermanfaat bagi masyarakat, memberikan kesejahteraan dan rasa aman.</li>
							</ol>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Tujuan dan Kurikulum -->
	<div class="register">
		<div class="container-fluid">
			<div class="row row-eq-height">
				<div class="col-lg-6 nopadding">
					<div class="search_section d-flex flex-column align-items-center justify-content-center">
						<h1 class="search_title">Tujuan</h1>
						<p>
							<b>UMUM : </b>
							<ol style="color: #000000; font-weight: 300; text-align: justify;">
								<li>Menyelenggarakan pelatihan dan kursus dalam bidang tata busana, busana industri, desain sketsa busana, dan lenan rumah tangga dengan teknik hias patchwork.</li>
								<li>Menghasilkan tenaga kerja dan wirausahawan yang kompeten sesuai dengan bidang pilihan yang di minati.</li>
								<li>Mempersiapkan dan membina tenaga kerja dan wirausahawan yang siap pakai dan berkualitas.</li>
							</ol>
						</p>
						<p>
							<b>KHUSUS : </b>
							<ol style="color: #000000; font-weight: 300; text-align: justify;">
								<li>Menghasilkan tenaga kerja yang kompeten di bidang tata busana (menjahit), busana industri, desain sketsa busana, lenan rumah tangga dengan teknik hias patchwork.</li>
								<li>Menghasilkan tenaga kerja dan wirausahawan yang siap bekerja baik di dunia industri dan berwirausaha.</li>
							</ol>
						</p>
					</div>
				</div>

				<div class="col-lg-6 nopadding">
					<div class="search_section d-flex flex-column align-items-center justify-content-center" style="background: #003e4e">
						<h1 class="search_title" style="color:#fff; padding-bottom: 15px;">Kurikulum</h1>
						<p style="color: #ffffff; font-weight: 300; line-height: 20px; text-align: justify;">
							Kurikulum yang digunakan oleh LKP/ LPK Dress Making adalah kurikulum berbasis KKNI (Standar Kerangka Kualifikasi Nasional Indonesia) dari Kementerian Pendidikan dan Kebudayaan serta kurikulum berbasis kompetensi SKKNI (Standar Kompetensi Kerja Nasional Indonesia) dari Kementerian Ketenagakerjaan. Selain itu, LKP/ LPK Dress Making dalam proses pembelajarannya melakukan penggabungan dan perubahan terhadap kurikulum tersebut agar sesuai dengan standar lulusan yang memenuhi kriteria dunia usaha/ industri. 
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Profil Pimpinan -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Profil Pimpinan</h1>
					</div>
				</div>
			</div>
			<div class="row general_row">
				<div class="col-lg-4 general_item">
					<div class="card" style="text-align: center; padding: 20px;">
						<div class="card_img">
							<img class="card-img-top trans_200" src="<?php echo base_url("asset/images/lpk/pimpinan_lembaga.jpg"); ?>" style="width:150px; height:150px; border-radius: 50%;">
						</div>
						<h1 style="color: #000; font-size: 20px; padding-top:15px;">Dewi Syamsiah, S.Pd.I</h1>
						<h2 style="color: #000; font-size: 15px; margin-top: -5px;">Pimpinan Lembaga</h2>
					</div>
				</div>
				<div class="col-lg-8 general_item" style="padding-left: 35px;">
					<h2>Identitas</h2>
					<p>
						<b>Nama lengkap :</b> Dewi Syamsiah, S.Pd.I<br>
						<b>Tempat/ tanggal lahir :</b> Bandung, 30 Oktober 1963<br>                                              
						<b>Jenis kelamin :</b> Perempuan<br>
						<b>Alamat :</b> Jl. Pesantren Cibabat Cimahi<br>
						<b>No. Telp. :</b> 081223246677<br>
						<b>Jabatan :</b> Pimpinan Lembaga
					</p>
					<hr>
					<h2>Pendidikan</h2>
					<p>
						<b>Pendidikan formal :</b><br>
						SI STAI YAMISA Lulus Tahun 2011 <br>
						<b>Pendidikan non-formal :</b><br>
						SI STAI YAMISA Lulus Tahun 2011 <br>
						Kursus menjahit MPWA Tingkat Terampil Lulus Tahun 1987<br>
						Kursus menjahit MPWA Tingkat Mahir  Lulus Tahun 1988
					</p>
					<hr>
					<h2>Riwayat Pekerjaan</h2>
					<p>
						Pimpinan LKP Dress Making tahun 1997– sekarang<br>
						Penguji LSK Tata Busana level II dan level III tahun 2010-sekarang<br>
						Pengelola Toko Fuji Grosir tahun 2000-sekarang<br>
					</p>
					<hr>
					<h2>Keterangan Organisasi</h2>
					<p>
						Ketua HIPKI DPC Kota Cimahi Tahun 2013- 2019<br> 
	 					Ketua DPD IPBI Kartini Provinsi Jawa  Barat Tahun 2019-2023<br>
	 					Penasehat HILSI Kota Cimahi Tahun 2020-  2024<br>
	 					Penasehat HILSI Kota Cimahi Tahun 2020-  2024
 					</p>
 					<hr>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>