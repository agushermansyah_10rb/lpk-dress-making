<?php $this->load->view('template/header'); ?>
<script src="<?php echo base_url("asset/js/pendaftaran.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/sweetalert/sweetalert.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/alert/alert.js"); ?>"></script>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Data galeri</h1>
			</div>
		</div>
	</div>

	<!-- Response status -->
    <?php 
        if ($_GET) {
            if (isset($_GET['status']) && isset($_GET['t'])) {
                $status = $_GET['status']; 
                $tipe = $_GET['t'];
                if ($tipe == 'a') {
                	$error = '';
	                if (isset($_GET['error'])) {
	                	$error = $_GET['error'];
	                }

	                $pesan = 'Kesalah Tidak Diketahui';
	                if ($error == '1') {
	                	$pesan = "Format Gambar Tidak Sesuai Ketentuan (.jpeg/.jpg/.png)";
	                }
	                else if ($error == '2') {
	                	$pesan = "Ukuran Gambar Terlalu Besar (Maksimal 1 MB)";
	                }
	                else if ($error == '3') {
	                	$pesan = "Gagal Mengupload Gambar";
	                }
	                else if ($error == '4') {
	                	$pesan = "Gambar Kosong";
	                }

	                if ($status == 'sukses') {
	                    echo "<script type='text/javascript'>success('Data Telah Tersimpan', 'admin-galeri', '', 'false');</script>";
	                } 
	                else if ($status == 'gagal') {
	                   	echo "<script type='text/javascript'>failed('$pesan', 'admin-galeri');</script>";
	                }
                }
                else if ($tipe == 'b') {
                	$pesan = 'Data Gagal Dihapus';
	                if ($status == 'sukses') {
	                    echo "<script type='text/javascript'>success('Data Telah Terhapus', 'admin-galeri', '', 'false');</script>";
	                } 
	                else if ($status == 'gagal') {
	                   	echo "<script type='text/javascript'>failed('$pesan', 'admin-galeri');</script>";
	                }
                }
            }
        }
    ?>

	<!-- Data Peserta -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Data galeri</h1>
						<p> </p>
					</div>
				</div>
			</div>

			<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
				<div class="col-md-12">

                    <div class="card" style="background: none">
                        <div class="card-body">
                        	<div class="table-responsive" style="margin-top: 15px;">
								<table id="zero_config" class="table table-borderless" style="width: 400px">
									<form action="<?php echo site_url('admin-galeri'); ?>" method="POST">
										<tr>
							  				<th style="border:none; padding-left: 0px; padding-top: 0px; padding-bottom: 0px;">
									 			<div class="form-group">
										    		<input type="text" class="form-control general_font_input" placeholder="Cari..." name="cari" id="cari" value="<?php echo isset($keyword) ? $keyword : '';?>">
										  		</div>
							  				</th>
							  				<th style="border:none; padding-left: 0px; padding-top: 0px; padding-bottom: 0px; padding-right: 0px;">
							  					<?php
							  						if (!$keyword) {
							  					?>
							  						<input class="btn btn-primary" type="submit" value="Cari">
							  					<?php
							  						}
							  						else {
							  					?>
							  						<button class="btn btn-danger" type="button" onclick="resetPencarian('<?php echo site_url("admin-galeri"); ?>')">Reset</button>
							  					<?php 
						  							}
						  						?>
							  				</th>
							  		</form>
							  				<th style="border:none; padding-left: 0px; padding-top: 0px; padding-bottom: 0px; padding-right: 0px">
							  					<button class="btn btn-success btn-act" data-toggle="modal" data-target="#modalGaleri" title="Edit" style="width: 80px;">Tambah</button>
							  				</th>
							  			</tr>	
						  			
								</table>

								 <!-- The Modal -->
								<div class="modal" id="modalGaleri">
								 	<div class="modal-dialog">
								    	<div class="modal-content">
									    	<form action="<?php echo site_url('admin/tambah_galeri'); ?>" method="POST" enctype="multipart/form-data">
										      	<!-- Modal Header -->
										      	<div class="modal-header">
										        	<h4 class="modal-title">Tambah Galeri</h4>
										        	<button type="button" class="close" data-dismiss="modal">&times;</button>
										      	</div>

										      	<!-- Modal body -->
										      	<div class="modal-body">
													<div class="form-group">
												   		<label for="foto">* Foto (.jpeg/.jpg/.png - maks 1 MB) : </label>
			    										<input type="file" class="form-control general_font_input" name="image" id="foto">
												  	</div>
													<div class="form-group">
												   		<label for="keterangan">* Keterangan: </label>
												    	<textarea class="form-control general_font_input" rows="4" name="keterangan" id="keterangan" required="true"></textarea>
												  	</div>
										      	</div>
									      	
										      	<!-- Modal footer -->
										      	<div class="modal-footer">
										        	<input type="submit" name="submit" value="Simpan" class="general_submit_button" style="height: 40px;">
										      	</div>
									      	</form>
								    	</div>
								  	</div>
								</div>

							  	<table id="zero_config" class="table table-bordered">
							  		<thead style="background: #606060; color: #FFFFFF">
									    <tr>
									        <th class="text-center">No</th>
									        <th class="text-center">Gambar</th>
									        <th>Keterangan</th>
									        <th class="text-center">Aksi</th>
									    </tr>
								    </thead>
							  		<?php 
							  			if ($data) {
								  			$i = 1;
								  			foreach ($data as $v) {
							  		?>
									    <tbody>
									      	<tr>
									        	<td class="text-center"><?php echo $i; ?></td>
									        	<td class="text-center">
									        		<img src="<?php echo base_url("asset/images/galeri/".$v->gambar); ?>" width="100px" height="50px">
									        	</td>
									        	<td><?php echo $v->keterangan; ?></td>
									        	<td class="text-center">
									        		<button class="btn btn-danger btn-act" onclick="ask_del('admin/hapus_galeri/<?php echo $v->g_id; ?>');" title="Hapus"><i class="fas fa-trash-alt"></i></button>
									        	</td>
									      	</tr>
									    </tbody>

									<?php 
											$i++;
											}
										}
										else {
									?>
									 	<tbody>
									      	<tr>
									        	<td colspan="4" style="text-align: center;">Tidak Ada Data</td>
									        </tr>
									    </tbody>
									<?php
										}
									?>
								</table>
								<?php echo 'Total : '.$total.' Item'; ?>
							</div>
                    	</div>
                   	</div>
				</div>
			</div>


			<div class="row general_row justify-content-md-center">
				<div class="col-lg-6">
					<?php 
						if ($total != 0) {
					?>
					    <ul class="pagination justify-content-center">
					        <?php
						        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 1;
						        $limit = 10;
						       	$start = ($page - 1) * $limit;
						        $q = isset($keyword) ? "?q=".$keyword : "";

						        if($page == 1) {
					        ?>
						        	<li class="page-item disabled"><a href="#" class="page-link">Pertama</a></li>
						          	<li class="page-item disabled"><a href="#" class="page-link">&laquo;</a></li>
					        <?php
						        } 
						        else { 
						        	// Jika page bukan page ke 1
						        	$link_prev = ($page > 1) ? $page - 1 : 1;
					        ?>
					        	<li><a href="<?php echo site_url('admin-galeri/1'.$q); ?>" class="page-link">Pertama</a></li>
					          	<li><a href="<?php echo site_url('gadmin-aleri/'.$link_prev.$q); ?>" class="page-link">&laquo;</a></li>
					        <?php
					        	}
					        ?>
					        
					        <?php
					        	$jumlah_page = ceil($total / $limit); // Hitung jumlah halamannya
					        	$jumlah_number = ($jumlah_page > 10) ? 10 : $jumlah_page; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
					        	$start_number = ($page > $jumlah_number) ? ($page - ($jumlah_number - 1)) : 1; // Untuk awal link number
					        	$end_number = ($page <= $jumlah_number) ? $jumlah_number : ($start_number + ($jumlah_number - 1)); // Untuk akhir link number
					        
						        for($i = $start_number; $i <= $end_number; $i++) {
						  			$link_active = ($page == $i) ? 'active' : '';
					        ?>
					        		<li class="page-item <?php echo $link_active; ?>">
					        			<a href="<?php echo site_url('admin-galeri/'.$i.$q); ?>" class="page-link"><?php echo $i; ?></a>
					        		</li>
					        <?php
					        	}
					        ?>
					        
					        <?php
						        if($page == $jumlah_page) { // Jika page terakhir
					        ?>
							        <li class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>
							        <li class="page-item disabled"><a href="#" class="page-link">Terakhir</a></li>
					        <?php
						        }
						        else { // Jika Bukan page terakhir
						          	$link_next = ($page < $jumlah_page) ? ($page) + 1 : $jumlah_page;
					        ?>
						         	<li><a href="<?php echo site_url('admin-galeri/'.$link_next.$q); ?>" class="page-link">&raquo;</a></li>
						          	<li><a href="<?php echo site_url('admin-galeri/'.$jumlah_page.$q); ?>" class="page-link">Terakhir</a></li>
					        <?php
					        	}
					        ?>
					    </ul>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>
