<?php $this->load->view('template/header'); ?>
<script src="<?php echo base_url("asset/js/sweetalert/sweetalert.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/alert/alert.js"); ?>"></script>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Kursus Online LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<!-- Response status -->
    <?php 
        if ($_GET) {
            if (isset($_GET['status'])) {
                $status = $_GET['status']; 
                $nid = '';
                if (isset($_GET['nid'])) {
                	$nid = $_GET['nid'];
                }
                $url = site_url('info/'.$nid);
                if ($status == 'sukses') {
                    echo "<script type='text/javascript'>success('Terimakasih Telah Menyelesaikan Kuis', '$url');</script>";
                } 
                else if ($status == 'gagal') {
                   	echo "<script type='text/javascript'>failed('Sorry, Undefined error!', 'supplier.php');</script>";
                }
            }
        }
    ?>

	<!-- Video -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Video Anda</h1>
						<p>Silakan tonton video terlebih dahulu dan ikuti kuis video tersebut!</p>
					</div>
				</div>
			</div>
			<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
				<div class="col-lg-6">
					<?php
						if ($data) {
							foreach($data as $v) {
					?>
						<div class="row video">
							<div class="col-lg-3 text-center video-image">
								<i class="fas fa-film i-video"></i>
							</div>
							<div class="col-lg-9">
								<h1><?php echo $v->judul; ?></h1>
								<p>
									Peretemuan ke <?php echo $v->tingkat; ?> | 
									<a href="<?php echo $v->link; ?>" target="_blank">Tonton Video</a> | 
									<?php 
										if ($v->pdf) {
									?>
										<a href="<?php echo base_url('/asset/file/'.$v->pdf);?>" target="_blank">Liat PDF</a> | 
									<?php 
										}
										if ($v->peserta_id && $v->peserta_id == $peserta_id) {
									?>
										Kuis
										<i class="far fa-check-circle"></i>
									<?php
										} else {
									?>
										<a href="<?php echo site_url('kuis/'.$v->video_id); ?>">Kuis</a>
									<?php
										} 
									?>
								</p>
							</div>
						</div>
					<?php
							}
						}
						else {
							echo "<h2 style='text-align:center;'> Tidak ada data </h2>";
						}
					?>
				</div>
			</div>
			<div class="row general_row justify-content-md-center">
				<div class="col-lg-6">
					<?php 
						if ($total != 0) {
					?>
					    <ul class="pagination justify-content-center">
					        <?php
						        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 1;
						        $limit = 5;
						       	$start = ($page - 1) * $limit;
						        
						        if($page == 1) {
					        ?>
						        	<li class="page-item disabled"><a href="#" class="page-link">Pertama</a></li>
						          	<li class="page-item disabled"><a href="#" class="page-link">&laquo;</a></li>
					        <?php
						        } 
						        else { 
						        	// Jika page bukan page ke 1
						        	$link_prev = ($page > 1) ? $page - 1 : 1;
					        ?>
					        	<li><a href="<?php echo site_url('kursus/1'); ?>" class="page-link">Pertama</a></li>
					          	<li><a href="<?php echo site_url('kursus/'.$link_prev); ?>" class="page-link">&laquo;</a></li>
					        <?php
					        	}
					        ?>
					        
					        <?php
					        	$jumlah_page = ceil($total / $limit); // Hitung jumlah halamannya
					        	$jumlah_number = ($jumlah_page > 10) ? 10 : $jumlah_page; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
					        	$start_number = ($page > $jumlah_number) ? ($page - ($jumlah_number - 1)) : 1; // Untuk awal link number
					        	$end_number = ($page <= $jumlah_number) ? $jumlah_number : ($start_number + ($jumlah_number - 1)); // Untuk akhir link number
					        
						        for($i = $start_number; $i <= $end_number; $i++) {
						  			$link_active = ($page == $i) ? 'active' : '';
					        ?>
					        		<li class="page-item <?php echo $link_active; ?>">
					        			<a href="<?php echo site_url('kursus/'.$i); ?>" class="page-link"><?php echo $i; ?></a>
					        		</li>
					        <?php
					        	}
					        ?>
					        
					        <?php
						        if($page == $jumlah_page) { // Jika page terakhir
					        ?>
							        <li class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>
							        <li class="page-item disabled"><a href="#" class="page-link">Terakhir</a></li>
					        <?php
						        }
						        else { // Jika Bukan page terakhir
						          	$link_next = ($page < $jumlah_page) ? ($page) + 1 : $jumlah_page;
					        ?>
						         	<li><a href="<?php echo site_url('kursus/'.$link_next); ?>" class="page-link">&raquo;</a></li>
						          	<li><a href="<?php echo site_url('kursus/'.$jumlah_page); ?>" class="page-link">Terakhir</a></li>
					        <?php
					        	}
					        ?>
					    </ul>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>