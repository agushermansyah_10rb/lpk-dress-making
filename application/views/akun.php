<?php $this->load->view('template/header'); ?>
<script src="<?php echo base_url("asset/js/sweetalert/sweetalert.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/alert/alert.js"); ?>"></script>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Detail Peserta</h1>
			</div>
		</div>
	</div>

	<!-- Response status -->
    <?php 
        if ($_GET) {
            if (isset($_GET['status'])) {
                $status = $_GET['status']; 
                $error = '';
                $p_id = ($this->uri->segment(2)) ? $this->uri->segment(2) : '';

                if (isset($_GET['error'])) {
                	$error = $_GET['error'];
                }

                $pesan = 'Kesalah Tidak Diketahui';
                if ($error == '1') {
                	$pesan = "Username Sudah Ada";
                }
                else if ($error == '2') {
                	$pesan = "Format Gambar Tidak Sesuai Ketentuan (.jpeg/.jpg/.png)";
                }
                else if ($error == '3') {
                	$pesan = "Ukuran Gambar Terlalu Besar (Maksimal 500Kb)";
                }
                else if ($error == '4') {
                	$pesan = "Gagal Mengupload Gambar";
                }

                if ($status == 'sukses') {
                    echo "<script type='text/javascript'>success('Data Telah Tersimpan', '$p_id', '', 'false');</script>";
                } 
                else if ($status == 'gagal') {
                   	echo "<script type='text/javascript'>failed('$pesan', '$p_id');</script>";
                }
            }
        }
    ?>

	<!-- Identitas Lembaga -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<!-- <h1></h1> -->
						<!-- <p></p> -->
					</div>
				</div>
			</div>
			
			<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
				<div class="col-lg-8">
					<?php 
						if ($data) {
							$v = $data;
					?>
					<div class="row justify-content-md-center">
						<div class="col-lg-12 d-flex justify-content-center">
							<div class="wrapper-profil d-flex justify-content-center">
								<?php 
									if ($v->foto_profil) {
								?>
									<img src="<?php echo base_url('asset/images/pendaftaran/'.$v->foto_profil);?>">
								<?php
									}
									else {
								?>
									<img src="<?php echo base_url('asset/images/pendaftaran/user.png');?>" style="width: 125px; height: 125px; margin-top: 20px">
								<?php 
									}
								?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<h2 style="margin-bottom: -2px"><?php echo $v->nama_lengkap; ?></h2>
							<?php 
								if ($v->status == 'proses') {
									echo "<p>Peserta Sedang Dalam Proses</p>";
								}
								else if ($v->status == 'disetujui') {
									echo "<p>Peserta Telah Disetujui</p>";
								}
								else if ($v->status == 'ditolak') {
									echo "<p>Peserta Telah Ditolak</p>";
								}
							?>
						</div>
					</div>
					<div class="row general_row justify-content-md-center" style="margin-top: 10px;">
						<div class="col-lg-8">
							<form action="<?php echo site_url('admin/edit_akun'); ?>" method="POST" enctype="multipart/form-data">
								<div class="form-group">
							   		<label for="nama">Nama Lengkap</label>
							   		<!-- Hidden Field -->
							    	<input type="hidden" class="form-control general_font_input" name="p_id" id="p_id" value="<?php echo $v->p_id; ?>">
							    	<input type="hidden" class="form-control general_font_input" name="peserta_id" id="peserta_id" value="<?php echo $v->peserta_id; ?>">
							    	<input type="hidden" class="form-control general_font_input" name="foto_profil" id="foto_profil" value="<?php echo $v->foto_profil; ?>">
							    	<input type="hidden" class="form-control general_font_input" name="nama_lengkap_old" id="nama_lengkap_old" value="<?php echo $v->nama_lengkap; ?>">
							    	<!-- End Hidden -->
							    	<input type="text" class="form-control general_font_input" name="nama_lengkap" id="nama_lengkap" value="<?php echo $v->nama_lengkap; ?>" required="true">
							  	</div>
								<div class="form-group">
							   		<label for="username">Username</label>
							    	<input type="text" class="form-control general_font_input" name="username" id="username" value="<?php echo $v->username; ?>" required="true">
							  	</div>
							  	<div class="form-group">
							   		<label for="password">Kata Sandi </label>
							    	<div class="input-group" id="show_hide_password">
								     	<input type="password" class="form-control general_font_input" name="password" id="password" value="<?php echo $v->password; ?>" required="true">
								      	<div class="input-group-addon">
								        	<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
								      	</div>
								    </div>
							  	</div>
							  	<div class="form-group">
							   		<label for="foto">* Foto (Opsional - .jpeg/.jpg/.png - maks 500Kb) : </label>
							    	<input type="file" class="form-control general_font_input" name="image" id="foto">
							  	</div>
							  	<div class="form-group">
							   		<label for="new_password">Kata Sandi Baru</label>
							    	<div class="input-group" id="show_hide_password2">
								     	<input type="password" class="form-control general_font_input" name="new_password" id="new_password">
								      	<div class="input-group-addon">
								        	<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
								      	</div>
								    </div>
							  	</div>
							  	<div class="form-group">
							   		<label for="kon_new_password">Konfirmasi Kata Sandi Baru</label>
							    	<div class="input-group" id="show_hide_password3">
								     	<input type="password" class="form-control general_font_input" name="kon_new_password" id="kon_new_password">
								      	<div class="input-group-addon">
								        	<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
								      	</div>
								    </div>
							  	</div>
								<input type="submit" name="submit" value="Simpan" id="submit" class="general_submit_button">
							</form>
					  	</div>
					</div>
					<?php
						}
						else {
							echo "<h2 style='text-align:center;'> Tidak ada data </h2>";
						}
					?>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>
<script src="<?php echo base_url("asset/js/akun.js"); ?>"></script>
