<?php $this->load->view('template/header'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/styles/news_post_responsive.css'); ?>">
<div class="super_container">
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Berita LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<div class="news">
		<div class="container">
			<?php 
				if (!$data) {
			?>
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Berita Kami</h1>
					</div>
				</div>
			</div>
			<?php
				}
			?>
			<div class="row general_row justify-content-md-center">
				<div class="col-lg-8">
					<div class="news_post_container">
						<?php 
							if ($data) {
								$v = $data;
						?>
						<!-- News Post -->
						<div class="news_post">
							<?php 
								if ($v->gambar) {
							?>
								<img src="<?php echo base_url("asset/images/berita/".$v->gambar); ?>" alt="Dress Making" class="img-fluid mx-auto d-block">
							<?php 
								}
							?>
							<div class="news_post_top d-flex flex-column flex-sm-row">
								<div class="news_post_date_container">
									<div class="news_post_date d-flex flex-column align-items-center justify-content-center">
										<div>
											<?php
												$dt = new DateTime($v->dibuat_pada); 
												echo $dt->format('d'); 
											?>	
										</div>
										<div>
											<?php
												$dt = new DateTime($v->dibuat_pada); 
												echo $dt->format('M'); 
											?>	
										</div>
									</div>
								</div>
								<div class="news_post_title_container">
									<div class="news_post_title">
										<a href="<?php echo site_url('berita-detail/'.$v->berita_id);?>"><?php echo $v->judul; ?></a>
									</div>
									<div class="news_post_meta">
										<span class="news_post_author"><a href="#">Oleh <?php echo $v->penulis; ?></a></span>
									</div>
								</div>
							</div>
							<div class="news_post_text">
								<?php echo preg_replace("/<p>/", "<p style='text-align: justify'>", $v->teks); ?>
							</div>
						</div>
						<?php 
							} 
							else {
				  				echo "<h2 style='text-align:center;'> Tidak ada data </h2>";
				  		}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>