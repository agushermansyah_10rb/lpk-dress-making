<script src="<?php echo base_url("asset/js/jquery-3.2.1.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/styles/bootstrap4/popper.js"); ?>"></script>
<script src="<?php echo base_url("asset/styles/bootstrap4/bootstrap.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/greensock/TweenMax.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/greensock/TimelineMax.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/scrollmagic/ScrollMagic.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/greensock/animation.gsap.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/greensock/ScrollToPlugin.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/OwlCarousel2-2.2.1/owl.carousel.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/scrollTo/jquery.scrollTo.min.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/easing/easing.js"); ?>"></script>
<script src="<?php echo base_url("asset/plugins/datepicker/js/bootstrap-datepicker.js"); ?>"></script>
<script src="<?php echo base_url("asset/js/custom.js"); ?>"></script>

</body>
</html>