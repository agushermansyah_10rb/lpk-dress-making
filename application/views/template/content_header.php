<!-- Header -->
<header class="header d-flex flex-row">
	<div class="header_content d-flex flex-row align-items-center">
		<!-- Logo -->
		<div class="logo_container">
			<div class="logo">
				<img src="<?php echo base_url('asset/images/lpk/logo.jpg'); ?>" alt="">
				<span>Dress Making</span>
			</div>
		</div>

		<!-- Main Navigation -->
		<nav class="main_nav_container">
			<div class="main_nav">
				<ul class="main_nav_list">
					<li class="main_nav_item"><a href="<?php echo site_url('beranda'); ?>">Beranda</a></li>
					<li class="main_nav_item"><a href="<?php echo site_url('profil'); ?>">Profil</a></li>
					<li class="main_nav_item">
						<div class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						    	Kursus
						    </a>
						    <div class="dropdown-menu" style="margin-top: 10px;">
						      	<a href="<?php echo site_url('program'); ?>" class="dropdown-item" >Program Kursus</a>
						      	<a href="<?php echo site_url('kursus'); ?>" class="dropdown-item">Kursus Online</a>
						  	</div>
						</div>
					</li>
					<li class="main_nav_item"><a href="<?php echo site_url('galeri'); ?>">Galeri</a></li>
					<li class="main_nav_item"><a href="<?php echo site_url('berita'); ?>">Berita</a></li>
					<li class="main_nav_item"><a href="<?php echo site_url('kontak'); ?>">Kontak</a></li>
				</ul>
			</div>
		</nav>
	</div>
	<div class="header_side d-flex flex-row justify-content-center align-items-center">
		<?php
			$userdata = $this->session->userdata();
			if(array_key_exists("status", $userdata)) {
				$p_id = $userdata['p_id'];
				$peserta_id = $userdata['peserta_id'];
				$status = $userdata['status'];
				$username = $userdata['username'];
				$tipe = $userdata['tipe'];
		?>
			<div class="dropdown float-right">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style="background: #003e4e; box-shadow: none; border:none;">
			    	<?php echo strlen($username) > 12 ? 'Hi, '.substr($username, 0, 12).'...' : 'Hi, '.$username; ?>
			  	</button>
			  	<div class="dropdown-menu dropdown-menu-right">
			  		<?php if ($tipe == 'admin') {
			  		?>
			  			<a class="dropdown-item" href="<?php echo site_url('menu'); ?>">Menu</a>
			  			<a class="dropdown-item" href="<?php echo site_url('akun'); ?>">Akun</a>
			    		<a class="dropdown-item" href="<?php echo site_url('login/logout'); ?>">Logout</a>
			    	<?php 
			    	} 
			    	else { 
			    	?>
			    		<a class="dropdown-item" href="<?php echo site_url('akun'); ?>">Akun</a>
			    		<a class="dropdown-item" href="<?php echo site_url('nilai'); ?>">Nilai</a>
			    		<a class="dropdown-item" href="<?php echo site_url('login/logout'); ?>">Logout</a>
			    	<?php 
			    		} 
			    	?>
			  	</div>
			</div>
		<?php
			}
			else {
		?>
			<a href="<?php echo site_url('daftar'); ?>" style="color: #FFFFFF">
				<i class="fas fa-paper-plane"></i>&nbsp;&nbsp;Pendaftaran Online
			</a>
		<?php
			}
		?>
	</div>

	<!-- Hamburger -->
	<div class="hamburger_container">
		<i class="fas fa-bars trans_200"></i>
	</div>

</header>

<!-- Menu -->
<div class="menu_container menu_mm">

	<!-- Menu Close Button -->
	<div class="menu_close_container">
		<div class="menu_close"></div>
	</div>

	<!-- Menu Items -->
	<div class="menu_inner menu_mm">
		<div class="menu menu_mm" style="width: 85%">
			<ul class="menu_list menu_mm">
				<li>
					<a href="<?php echo site_url('beranda'); ?>">
						<div class="btn-nav"><i class="fas fa-home"></i> &nbsp;Beranda</div>
					</a>
				</li>
				<li>
					<a href="<?php echo site_url('profil'); ?>">
						<div class="btn-nav"><i class="fas fa-user-circle"></i> &nbsp;Profil</div>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="dropdown">
						<div class="btn-nav dropdown-toggle">
							<i class="fas fa-cut"></i> &nbsp;
					    	Kursus
						</div>
					</a>
					 <div class="dropdown-menu">
				      	<a href="<?php echo site_url('program'); ?>" class="dropdown-item" >Program Kursus</a>
				      	<a href="<?php echo site_url('kursus'); ?>" class="dropdown-item">Kursus Online</a>
				  	</div>
				</li>
				<li>
					<a href="<?php echo site_url('galeri'); ?>">
						<div class="btn-nav"><i class="fas fa-image"></i> &nbsp;Galeri</div>
					</a>
				</li>
				<li>
					<a href="<?php echo site_url('berita'); ?>">
						<div class="btn-nav"><i class="far fa-newspaper"></i> &nbsp;Berita</div>
					</a>
				</li>
				<li>
					<a href="<?php echo site_url('kontak'); ?>">
						<div class="btn-nav"><i class="fas fa-phone-square"></i> &nbsp;Kontak</div>
					</a>
				</li>
				<li>
					<?php
						$userdata = $this->session->userdata();
						if(array_key_exists("status", $userdata)) {
							$peserta_id = $userdata['peserta_id'];
							$status = $userdata['status'];
							$username = $userdata['username'];
							$tipe = $userdata['tipe'];
					?>
					<div class="dropdown float-right">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" style="background: none; box-shadow: none; border:none; color: #1a1a1a; margin-top: 10px; font-size: 18px; font-weight: 600;">
					    	<?php echo strlen($username) > 12 ? 'Hi, '.substr($username, 0, 12).'...' : 'Hi, '.$username; ?>
					  	</button>
					  	<div class="dropdown-menu dropdown-menu-right">
					  		<?php if ($tipe == 'admin') {
					  		?>
					  			<a class="dropdown-item" href="<?php echo site_url('menu'); ?>">Menu</a>
					  			<a class="dropdown-item" href="<?php echo site_url('profil'); ?>">Akun</a>
					    		<a class="dropdown-item" href="<?php echo site_url('login/logout'); ?>">Logout</a>
					    	<?php 
					    	} 
					    	else { 
					    	?>
					    		<a class="dropdown-item" href="<?php echo site_url('profil'); ?>">Akun</a>
					    		<a class="dropdown-item" href="<?php echo site_url('nilai'); ?>">Nilai</a>
					    		<a class="dropdown-item" href="<?php echo site_url('login/logout'); ?>">Logout</a>
					    	<?php 
					    		} 
					    	?>
					  	</div>
					</div>
				<?php 
					} 
				?>
				</li>
			</ul>
		</div>
	</div>
</div>