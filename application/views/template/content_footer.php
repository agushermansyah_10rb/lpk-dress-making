<footer class="footer">
	<div class="container">
		<!-- Footer Copyright -->
		<div class="footer_bar">
			<div class="footer_copyright">
				<span>
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> <b>LKP/LPK Dress Making</b> <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
				</span>
			</div>
		</div>
	</div>
</footer>