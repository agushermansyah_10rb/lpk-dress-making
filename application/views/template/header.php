<!DOCTYPE html>
<html lang="en">
<head>
<title>Dress Making</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Course Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/styles/gallery.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/styles/bootstrap4/bootstrap.min.css"); ?>">
<link href="<?php echo base_url("asset/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css"); ?>" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/plugins/OwlCarousel2-2.2.1/owl.carousel.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/plugins/OwlCarousel2-2.2.1/owl.theme.default.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/plugins/OwlCarousel2-2.2.1/animate.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/plugins/datepicker/css/bootstrap-datepicker.min.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/styles/main_styles.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("asset/styles/responsive.css"); ?>">
<link rel="icon" type="image/png" href="<?php echo base_url("asset/images/lpk/logo.jpg"); ?>">
</head>
<body>