<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	<div class="home">
		<!-- Hero Slider -->
		<div class="hero_slider_container">
			<div class="hero_slider owl-carousel">
				
				<!-- Hero Slide -->
				<div class="hero_slide">
					<div class="hero_slide_background" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
					<div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
						<div class="hero_slide_content text-center">
							<h1 data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">
								<span>LKP/LPK</span> Dress Making
							</h1>
							<h2 data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">
								Lembaga Pendidikan & Pelatihan Menjahit
							</h2>
						</div>
					</div>
				</div>
				
				<!-- Hero Slide -->
				<div class="hero_slide">
					<div class="hero_slide_background" style="background-image:url(<?php echo base_url("asset/images/slider_background2.jpg"); ?>"></div>
					<div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
						<div class="hero_slide_content text-center">
							<h1 data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">Dapatkan <span>Pendidikan</span> Hari Ini!</h1>
						</div>
					</div>
				</div>
			</div>

			<div class="hero_slider_left hero_slider_nav trans_200"><span class="fas fa-chevron-circle-left trans_200"></span></div>
			<div class="hero_slider_right hero_slider_nav trans_200"><span class="fas fa-chevron-circle-right trans_200"></span></div>
		</div>
	</div>

	<div class="hero_boxes">
		<div class="hero_boxes_inner">
			<div class="container">
				<div class="row justify-content-md-center">
					<div class="col-lg-3 hero_box_col">
						<a href="<?php echo site_url('daftar'); ?>">
							<div class="hero_box d-flex flex-row align-items-center justify-content-start" style="height: 120px">
								<div class="hero_box_content">
									<h2 class="hero_box_title">Pendaftaran</h2>
									<div class="hero_box_link">Lihat Detail</div>
								</div>
							</div>
						</a>
					</div>

					<?php
						$userdata = $this->session->userdata();
						if(array_key_exists("status", $userdata)) {
							echo "";
						} else {
					?>
						<div class="col-lg-3 hero_box_col">
							<a href="<?php echo site_url('login'); ?>">
								<div class="hero_box d-flex flex-row align-items-center justify-content-start" style="height: 120px">
									<div class="hero_box_content">
										<h2 class="hero_box_title">Login</h2>
										<div class="hero_box_link">Lihat Detail</div>
									</div>
								</div>
							</a>
						</div>
					<?php
						}
					?>

					<div class="col-lg-3 hero_box_col">
						<a href="<?php echo site_url('kepemilikan'); ?>">
							<div class="hero_box d-flex flex-row align-items-center justify-content-start" style="height: 120px">
								<div class="hero_box_content">
									<h2 class="hero_box_title">Kepemilikan</h2>
									<div class="hero_box_link">Lihat Detail</div>
								</div>
							</div>
						</a>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- Sejarah -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Sejarah LKP/LPK Dress Making</h1>
					</div>
				</div>
			</div>
			<div class="row general_row">
				<div class="col-lg-12 general_item">
					<p>
						Lembaga pelatihan memiliki tujuan akhir dimana menciptakan karakter-karakter yang memiliki keahlian dan kompeten dalam bidang tata busana (menjahit) pada khususnya dan umumnya dalam berbagai hal. LKP/ LPK Dress Making  ini didirikan pada dasarnya berlatar belakang pada banyaknya pengganguran dan sumber daya manusia yang belum menemukan keahlian pada pribadinya masing-masing. Selain itu, untuk membantu masyarakat menemukan keahlian khususnya di bidang tata busana, juga membantu mengurangi tingkat penggangguran dikota Cimahi.
					</p>
				</div>
			</div>
		</div>
	</div>

</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>