<?php $this->load->view('template/header'); ?>
<div class="super_container">
	<?php $this->load->view('template/content_header'); ?> 
	
	<div class="home_general">
		<div class="home_general_background_container prlx_parent">
			<div class="home_general_background prlx" style="background-image:url(<?php echo base_url("asset/images/slider_background.jpg"); ?>"></div>
		</div>
		<div class="hero_slider_container d-flex flex-column align-items-center justify-content-center">
			<div class="home_general_content2 text-center">
				<h1>Kontak LKP/LPK Dress Making</h1>
			</div>
		</div>
	</div>

	<!-- Kontak -->
	<div class="general page_section">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title text-center">
						<h1>Hubungi Kami</h1>
					</div>
				</div>
			</div>
			<div class="row general_row">
				<div class="col-lg-3 general_item">
					<a href="https://api.whatsapp.com/send?phone=6282295205440" target="_blank">
						<div class="row kontak_row">
							<div class="col-lg-2">
								<h2><i class="fab fa-whatsapp" style="font-size: 28px"></i></h2>
							</div>
							<div class="col-lg-10" style="padding-left: 10px;">
								<h2 style="margin-bottom: 0">WhatsApp</h2>
								+62 82295205440 (Ramlan) | 085222575577 (Bu Ira)
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-3 general_item">
					<a href="https://www.gmail.com" target="_blank">
						<div class="row kontak_row">
							<div class="col-lg-2">
								<h2><i class="far fa-envelope" style="font-size: 28px"></i></h2>
							</div>
							<div class="col-lg-10" style="padding-left: 10px;">
								<h2 style="margin-bottom: 0">Email</h2>
								dressmaking1997@gmail.com
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-3 general_item">
					<a href="https://www.instagram.com/dressmaking97" target="_blank">
						<div class="row kontak_row">
							<div class="col-lg-2">
								<h2><i class="fab fa-instagram" style="font-size: 28px"></i></h2>
							</div>
							<div class="col-lg-10" style="padding-left: 10px;">
								<h2 style="margin-bottom: 0">Instagram</h2>
								@dressmaking97
							</div>
						</div>
					</a>
				</div>
				<div class="col-lg-3 general_item">
					<a href="https://www.facebook.com/dress.making.10" target="_blank">
						<div class="row kontak_row">
							<div class="col-lg-2">
								<h2><i class="fab fa-facebook" style="font-size: 28px"></i></h2>
							</div>
							<div class="col-lg-10" style="padding-left: 10px;">
								<h2 style="margin-bottom: 0">Facebook</h2>
								Dress Making
							</div>
						</div>
					</a>
				</div>
			</div>

			<div class="row general_row">
				<div class="col-lg-3 general_item" style="margin-top: -50px;">
					<div class="row kontak_row">
						<div class="col-lg-2">
							<h2><i class="fas fa-map-marker-alt" style="font-size: 28px"></i></h2>
						</div>
						<div class="col-lg-10" style="padding-left: 10px;">
							<h2 style="margin-bottom: 0">Alamat</h2>
							<p style="line-height: 20px">Jl. Sentral No. 28 RT 01 RW 12 Kel. Cibabat Kec. Cimahi Utara Kota Cimahi Provinsi Jawa Barat, 40513</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('template/content_footer'); ?>
<?php $this->load->view('template/footer'); ?>