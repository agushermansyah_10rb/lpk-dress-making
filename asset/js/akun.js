$(document).ready(function() {
    $("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash" );
            $('#show_hide_password i').removeClass( "fa-eye" );
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash" );
            $('#show_hide_password i').addClass( "fa-eye" );
        }
    });

    $("#show_hide_password2 a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password2 input').attr("type") == "text"){
            $('#show_hide_password2 input').attr('type', 'password');
            $('#show_hide_password2 i').addClass( "fa-eye-slash" );
            $('#show_hide_password2 i').removeClass( "fa-eye" );
        }else if($('#show_hide_password2 input').attr("type") == "password"){
            $('#show_hide_password2 input').attr('type', 'text');
            $('#show_hide_password2 i').removeClass( "fa-eye-slash" );
            $('#show_hide_password2 i').addClass( "fa-eye" );
        }
    });

    $("#show_hide_password3 a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password3 input').attr("type") == "text"){
            $('#show_hide_password3 input').attr('type', 'password');
            $('#show_hide_password3 i').addClass( "fa-eye-slash" );
            $('#show_hide_password3 i').removeClass( "fa-eye" );
        }else if($('#show_hide_password3 input').attr("type") == "password"){
            $('#show_hide_password3 input').attr('type', 'text');
            $('#show_hide_password3 i').removeClass( "fa-eye-slash" );
            $('#show_hide_password3 i').addClass( "fa-eye" );
        }
    });

    $("#password").on('change', function(event) {
        event.preventDefault();
        validatePasswordStrength();
    });

    $("#new_password").on('change', function(event) {
        event.preventDefault();
        validateNewPasswordStrength();
        validatePasswordConfirm();
    });

    $("#kon_new_password").on('keyup', function(event) {
        event.preventDefault();
        validateKonPasswordStrength();
        validatePasswordConfirm();
    });
});

var password = document.getElementById("password");
var new_password = document.getElementById("new_password");
var confirm_password = document.getElementById("kon_new_password");

function validatePasswordStrength() {
  if((password.value).length < 8) {
    password.setCustomValidity("Minimal Kata Sandi 8 Karakter");
  } else {
    password.setCustomValidity('');
  }
}

function validateNewPasswordStrength() {
  if((new_password.value).length < 8) {
    new_password.setCustomValidity("Minimal Kata Sandi 8 Karakter");
  } else {
    new_password.setCustomValidity('');
  }
}

function validateKonPasswordStrength() {
  if((confirm_password.value).length < 8) {
    confirm_password.setCustomValidity("Minimal Kata Sandi 8 Karakter");
  } else {
    confirm_password.setCustomValidity('');
  }
}

function validatePasswordConfirm(){
  if(new_password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Kata Sandi Tidak Sesuai");
  } else {
    confirm_password.setCustomValidity('');
  }
}
