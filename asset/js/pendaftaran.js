function tambahPengguna(peserta_id, email) {
    document.getElementById("peserta_id").value = peserta_id;
    document.getElementById("username").value = generateUsername(email);
    document.getElementById("password").value = generatePassword();
}

function tambahKeterangan(peserta_id) {
    document.getElementById("new_peserta_id").value = peserta_id;
}

function resetPencarian(location) {
    document.getElementById("cari").value = '';
    window.location.href = location;
}

function detailPeserta(location) {
    window.location.href = location;
}

function generatePassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

function generateUsername(email) {
	const em = email;
	const regex = /@.*/;
	username = em.replace(regex, '');
	return username;
}

function hidePass() {
  	var pass = document.getElementById("password");
		if (pass.type === "password") {
	    	pass.type = "text";
	  	} else {
	    	pass.type = "password";
	  	}
}

/* Peserta */
function nilaiPeserta(location) {
    window.location.href = location;
}

function resetPencarianPeserta(location) {
    document.getElementById("cari").value = '';
    document.getElementById("stat").value = '';
    window.location.href = location;
}

/* Pengguna */
function editAkun(location) {
    window.location.href = location;
}
