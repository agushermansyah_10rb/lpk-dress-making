function buatajax() {
  if (window.XMLHttpRequest) {
      return new XMLHttpRequest();
    }
  if (window.ActiveXObject) {
    return new ActiveXObject("Microsoft.XMLHTTP");
  }
  return null;
}

var ajaxku = buatajax();

function ajaxKota(base_url, id) {
  var url = base_url + "kabupaten/" + id.replace(/\|.*/i, "");
  console.log(url);
  ajaxku.onreadystatechange=kabupaten;
  ajaxku.open("GET",url,true);
  ajaxku.send(null);
}

function kabupaten(){
  var data;
  var jumlah;
  var i;
  if (ajaxku.readyState == 4) {
    data = JSON.parse(ajaxku.responseText);
    jumlah = data.length;
    if(jumlah > 0){
      removeKabupaten();
      removeKecamatan();
      removeKelurahan();
      for (i=0; i<jumlah; i++) {
        document.getElementById("kabupaten").append(new Option(data[i]['nama'], data[i]['id_kab'] + "|" + data[i]['nama']));
      }
    }
    else {
      removeKabupaten();
      removeKecamatan();
      removeKelurahan();
    }
  }
}

function ajaxKecamatan(base_url, id) {
  var url = base_url + "kecamatan/" + id.replace(/\|.*/i, "");
  ajaxku.onreadystatechange=kecamatan;
  ajaxku.open("GET",url,true);
  ajaxku.send(null);
}

function kecamatan(){
  var data;
  var jumlah;
  var i;
  if (ajaxku.readyState == 4) {
    data = JSON.parse(ajaxku.responseText);
    jumlah = data.length;
    if(jumlah > 0){
      removeKecamatan();
      removeKelurahan();
      for (i=0; i<jumlah; i++) {
        document.getElementById("kecamatan").append(new Option(data[i]['nama'], data[i]['id_kec'] + "|" + data[i]['nama']));
      }
    }
    else {
      removeKecamatan();
      removeKelurahan();
    }
  }
}

function ajaxKelurahan(base_url, id) {
  var url = base_url + "kelurahan/" + id.replace(/\|.*/i, "");
  ajaxku.onreadystatechange=kelurahan;
  ajaxku.open("GET",url,true);
  ajaxku.send(null);
}

function kelurahan() {
  var data;
  var jumlah;
  var i;
  if (ajaxku.readyState == 4){
    data = JSON.parse(ajaxku.responseText);
    jumlah = data.length;
    if(jumlah > 0) {
      removeKelurahan();
      for (i=0; i<jumlah; i++) {
        document.getElementById("kelurahan").append(new Option(data[i]['nama'], data[i]['id_kel'] + "|" + data[i]['nama']));
      }
    }
    else {
      removeKelurahan();
    }
  }
}

function removeKabupaten() {
  var select = document.getElementById("kabupaten");
      var length = select.options.length;
      for (i = length-1; i > 0; i--) {
        select.options[i] = null;
      }
}

function removeKecamatan() {
  var select = document.getElementById("kecamatan");
      var length = select.options.length;
      for (i = length-1; i > 0; i--) {
        select.options[i] = null;
      }
}

function removeKelurahan() {
  var select = document.getElementById("kelurahan");
      var length = select.options.length;
      for (i = length-1; i > 0; i--) {
        select.options[i] = null;
      }
}