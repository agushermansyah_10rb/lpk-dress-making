function buatajax() {
  	if (window.XMLHttpRequest) {
    	return new XMLHttpRequest();
  	}
  	if (window.ActiveXObject) {
    	return new ActiveXObject("Microsoft.XMLHTTP");
  	}
  	return null;
}

var ajaxku = buatajax();
var base_url = window.location.origin + "/" + window.location.pathname.split( '/' )[1] + "/";


function _ajaxKota() {
 	var id = document.getElementById("provinsi").value
 	var url = base_url + "kabupaten/" + id.replace(/\|.*/i, "");
    console.log(base_url);
  	ajaxku.onreadystatechange=_kabupaten;
  	ajaxku.open("GET",url,true);
  	ajaxku.send(null);
}

function _kabupaten() {
  	var data;
  	var jumlah;
  	var i;
  	if (ajaxku.readyState == 4) {
    	data = JSON.parse(ajaxku.responseText);
    	jumlah = data.length;
    	if(jumlah > 0) {
      		var kab = document.getElementById("kabupaten").value
      		removeKabupaten();
      		for (i=0; i<jumlah; i++) {
	      		if (data[i]['nama'] == kab) {
	        		document.getElementById("kabupaten").append(new Option(data[i]['nama'], data[i]['id_kab'] + "|" + data[i]['nama'], false, true));
	        	}
	        	else {
	        		document.getElementById("kabupaten").append(new Option(data[i]['nama'], data[i]['id_kab'] + "|" + data[i]['nama']));
	        	}
      		}
      		_ajaxKecamatan();
    	}
    	else {
      		removeKabupaten();
    	}
  	}
}


function _ajaxKecamatan() {
  	var id = document.getElementById("kabupaten").value
 	var url = base_url + "kecamatan/" + id.replace(/\|.*/i, "");
  	ajaxku.onreadystatechange=_kecamatan;
  	ajaxku.open("GET",url,true);
  	ajaxku.send(null);
}

function _kecamatan() {
	var data;
	var jumlah;
	var i;
	if (ajaxku.readyState == 4) {
    	data = JSON.parse(ajaxku.responseText);
    	jumlah = data.length;
    	if (jumlah > 0) {
    	var kec = document.getElementById("kecamatan").value
     	removeKecamatan();
      	for (i=0; i<jumlah; i++) {
      		if (data[i]['nama'] == kec) {
        		document.getElementById("kecamatan").append(new Option(data[i]['nama'], data[i]['id_kec'] + "|" + data[i]['nama'], false, true));
        	}
        	else {
        		document.getElementById("kecamatan").append(new Option(data[i]['nama'], data[i]['id_kec'] + "|" + data[i]['nama']));
        	}
      	}
      	_ajaxKelurahan();
      	
    }
    else {
      	removeKecamatan();
    	}
  	}
}

function _ajaxKelurahan() {
 	var id = document.getElementById("kecamatan").value
 	var url = base_url + "kelurahan/" + id.replace(/\|.*/i, "");
  	ajaxku.onreadystatechange=_kelurahan;
  	ajaxku.open("GET",url,true);
  	ajaxku.send(null);
}

function _kelurahan() {
	var data;
	var jumlah;
	var i;
	if (ajaxku.readyState == 4){
    	data = JSON.parse(ajaxku.responseText);
    	jumlah = data.length;
    	if(jumlah > 0) {
    		var kel = document.getElementById("kelurahan").value
	     	removeKelurahan();
	      	for (i=0; i<jumlah; i++) {
	      		if (data[i]['nama'] == kel) {
	        		document.getElementById("kelurahan").append(new Option(data[i]['nama'], data[i]['id_kel'] + "|" + data[i]['nama'], false, true));
	        	}
	        	else {
	        		document.getElementById("kelurahan").append(new Option(data[i]['nama'], data[i]['id_kel'] + "|" + data[i]['nama']));
	        	}
	      	}
   		}
    else {
    	removeKelurahan();
    	}
  	}
}

_ajaxKota();