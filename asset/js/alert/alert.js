function success(message, location, button, reload='true') {
    swal({
        title: "Berhasil!",
        text: message,
        icon: "success",
        button: button ? button : "OK",
        confirmButtonColor: 'red'

    })
    .then((value) => {
        // Replace current url
        if (reload == 'true') {
            window.location.href = location;
        } else {
             window.history.replaceState({}, document.title, location);
        }
        
    });
}

function failed(message, location) {
     swal({
        title: "Gagal!",
        text: message,
        icon: "error",
        button: "OK",
    })
    .then((value) => {
        // Replace current url
        window.history.replaceState({}, document.title, location);
    });
}

function ask_del(location) {
    swal({
        title: "Apakah Anda Yakin?",
        text: "Data Akan Dihapus Permanen",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            // Move other url
            window.location.href = location;
        } 
    });
}

function move(location) {
    // Move other url
    window.location.href = location;
}

